let
  nixpkgs = import
    (fetchTarball {
      url = "https://github.com/NixOS/nixpkgs/archive/22.11.tar.gz";
      sha256 = "sha256:11w3wn2yjhaa5pv20gbfbirvjq6i3m7pqrq2msf0g7cv44vijwgw";
    })
    { };

  kapack = import
    (nixpkgs.fetchgit {
      url = "https://github.com/oar-team/nur-kapack";
    sha256 = "0p19rncjq8jxmlgcw1i8117hf274kscapgrl6xa1p21cgz9lnhzc";
    rev = "c091728fbcddf76f2b386ce3777fe3440aa6116f";
    })
    { };

  myPythonPackages = with nixpkgs.python38Packages; [
    ipython
    pyyaml
    numpy
    pandas
    matplotlib
    pulp
    networkx
  ];
  python-with-my-packages = nixpkgs.python3.withPackages (p: myPythonPackages);
  myRPackges = with nixpkgs.rPackages; [
    dplyr
    tidyr
    ggplot2
    data_table
    gridExtra
    patchwork
    RColorBrewer
    scales
  ];
  myR = nixpkgs.rWrapper.override { packages = myRPackges; };

  # my-packages = rec {
  # define your own batsim version from batsim-4.0.0
  # (you can select another base, such as batsim-310 for batsim-3.1.0)
  my-batsim = kapack.batsim-400.overrideAttrs (old: rec {
    # define where to the source code of your own batsim version.
    # here, use a given commit on the official batsim git repository on framagit,
    # but you can of course use your own commit/fork instead.
    version = "c79d95851cd8f80089b7218a2cf85f4b26ebde5f";
    src = kapack.pkgs.fetchgit rec {
      url = "https://framagit.org/batsim/batsim.git";
      rev = version;
      sha256 = "0lr3vawdbmajisgvbj5cjqw0wfy9y990yhl20kplarambx40nplp";
    };
  });

  # define your own version of a scheduler, here from batsched-1.4.0
  my-batsched = kapack.batsched-140.overrideAttrs (old: rec {
    # (you can also your own commit/fork of batsched here)
    version = "b020e48b89a675ae681eb5bcead01035405b571e";
    src = kapack.pkgs.fetchgit rec {
      url = "https://framagit.org/batsim/batsched.git";
      rev = version;
      sha256 = "1dmx2zk25y24z3m92bsfndvvgmdg4wy2iildjmwr3drmw15s75q0";
    };
  });

  # can also be done on the pybatsim scheduler, here from pybatsim-3.2.0
  # (note that overridePythonAttrs is needed instead of overrideAttrs)
  my-pybatsim = kapack.pybatsim-320.overridePythonAttrs (old: rec {
    # (you can also your own commit/fork of pybatsim here)
    version = "fa6600eccd4e5ffbebfc7ecba05b64cf84c5e9d3";
    src = kapack.pkgs.fetchgit rec {
      url = "https://gitlab.inria.fr/batsim/pybatsim.git";
      rev = version;
      sha256 = "0nr8hqsx6y1k0lv82f4x3fjq6pz2fsd3h44cvnm7w21g4v3s6l6y";
    };
  });

in
{

  batshell = nixpkgs.mkShell {
    buildInputs = [
      my-batsim
      my-batsched
      my-pybatsim
      kapack.batexpe
      myPythonPackages
    ];
  };

  pyshell = nixpkgs.mkShell {
    buildInputs = [
      python-with-my-packages
      myPythonPackages
      kapack.procset
    ];
  };
  rshell = nixpkgs.mkShell {
    buildInputs = [
      myR
    ];
  };
}

