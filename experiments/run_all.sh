#!/bin/bash

PARAMETERS_FILE="./parameters_small.yaml" #"./parameters.yaml"
EXP_DESCRIPTION_PATH="./simulations/exp_description/"

# Preparing the directories
echo '===================================================='
echo 'Preparing the directories'
echo '===================================================='

#rm -rf ../results
#rm -rf simulations/exp_description
#rm -rf simulations/workloads
#rm -rf simulations/platforms
#rm -rf simulations/exp_out
#rm -rf ../analysis/figures
#rm -rf simulations/exp_description
#rm experiments_to_be_executed.json

#mkdir ../results
#mkdir simulations/exp_description
#mkdir simulations/workloads
#mkdir simulations/platforms
#mkdir simulations/exp_out
#mkdir ../analysis/figures

echo 'Done.'

## Prepare and Run
echo '===================================================='
echo 'Installing the environment'
echo '===================================================='
nix-build environment.nix
echo 'Done.'

# Prepare and Run
echo '===================================================='
echo 'Preparing and the experiments'
echo '===================================================='
nix-shell environment.nix -A pyshell --run "python3 scripts/generate_experiments.py parameters_small.yaml" --pure
echo 'Done.'

echo '===================================================='
echo 'Running the experiments'
echo '===================================================='
for entry in "$EXP_DESCRIPTION_PATH"*
do
  nix-shell environment.nix -A batshell --run "robin $entry" --pure 
done
echo 'Done.'

# Pre-proccess the simulations outputs
echo '===================================================='
echo 'Running the pre-processing of the outputs'
echo '===================================================='
nix-shell environment.nix -A pyshell --run "python3 scripts/pre_process_outputs.py parameters_small.yaml" --pure
echo 'Done.'

### Run the tests
echo '===================================================='
echo 'Running the tests'
echo '===================================================='
#nix-shell environment.nix -A pyshell --run "python3 scripts/run_tests.py parameters_small.yaml" --pure
echo 'Done.'

# Run the analysis
echo '===================================================='
echo 'Running the analysis of the results'
echo '===================================================='
#cd ../analysis/
#nix-shell ../experiments/environment.nix -A rshell --run "Rscript analysis.r" --pure
#cd ../experiments/
echo 'Done.'