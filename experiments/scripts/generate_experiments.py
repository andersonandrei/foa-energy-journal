import os
import subprocess
import json
import yaml
from yaml.loader import SafeLoader
import sys


def generate_experiment_description(
        exp_description_path,
        exp_out_path,
        scheduling_policies,
        scheduler_path,
        platforms_path, 
        platform_names,
        workloads_path, 
        workload_names,
        workload_sizes,
        random_seeds,
        profiles_description_per_machine_path,
        container_description_path,
        simulation_flags,
        batsim_failure_timeout,
        batsim_ready_timout,
        batsim_simulation_timeout,
        batsim_success_timeout):#, 
        #optimization_factors):

    print("--------------------- Generating the experiments description files ---------------------")

    failure_timeout = batsim_failure_timeout
    ready_timout = batsim_ready_timout
    schedcmd_profile_description_per_machine_path = profiles_description_per_machine_path
    schedcmd_container_description_path = container_description_path
    simulation_timeout = batsim_simulation_timeout
    success_timeout = batsim_success_timeout

    batcmd_p = ''
    batcmd_w = ''
    batcmd_e = ''
    schedcmd_workload_size = ''
    schedcmd_random_seed = ''
    schedcmd_scheduler = ''

    for scheduling_policy in scheduling_policies:
        for workload_size in workload_sizes:
            for platform in platform_names:
                for seed in random_seeds:
                    platform_description = "_".join(platform.split(".xml")[0].split("_")[0:])
                    platform_size = "_".join(platform.split(".xml")[0].split("_")[1:2])
                    platform_seed = "_".join(platform.split(".xml")[0].split("_")[5:])

                    #workload = "workload_size_" + str(workload_size) + "_" + platform_size + "_" + platform_seed + ".json"
                    workload = "workload_size_" + str(workload_size) + "_res_" + platform_size + "_seed_" + str(seed) + ".json"
                    workload_description = "_".join(workload.split(".json")[0].split("_")[1:3])
                    
                    output_file = exp_description_path + 'exp_' + scheduling_policy + "_" + workload_description + "_" + platform_description + "_" + str(seed) + ".yaml"
                    print(output_file)

                    output_dir = exp_out_path + "exp_" + scheduling_policy + "_" + workload_description + "_" + platform_description + "_" + str(seed)
                    batcmd_p = platforms_path + platform
                    batcmd_w = workloads_path + workload
                    print(batcmd_w)
                    batcmd_e = output_dir + "/out"
                    batcmd_f = simulation_flags

                    schedcmd_scheduler = scheduler_path + scheduling_policy + ".py"
                    schedcmd_workload_size = workload.split("_")[2]
                    schedcmd_random_seed = str(seed) #platform.split("_")[-1].split(".xml")[0]

                    schedcmd_str = "pybatsim " + schedcmd_scheduler + " -o '{" + \
                        '"profiles_description_per_machine_path":"' + schedcmd_profile_description_per_machine_path + '",' + \
                        '"container_description_path":"' + schedcmd_container_description_path + '",' + \
                        '"download_info_csv_path":"' + output_dir + '/",' + \
                        '"workload_size":' + schedcmd_workload_size + ',' + \
                        '"random_seed":' + schedcmd_random_seed + "}'"
                    
                    robing_file_data = {}
                    robing_file_data["batcmd"] = "batsim" + " -p " + batcmd_p + " -w " + batcmd_w + " -e " + batcmd_e + " " + batcmd_f
                    robing_file_data["failure-timeout"] = failure_timeout
                    robing_file_data["output-dir"] = output_dir
                    robing_file_data["ready-timeout"] = ready_timout
                    robing_file_data["schedcmd"] = schedcmd_str
                    robing_file_data["simulation-timeout"] = simulation_timeout
                    robing_file_data["success-timeout"] = success_timeout
                    
                    print("Experiment generated: ", robing_file_data)
                    with open(output_file, 'w') as file:
                        yaml.dump(robing_file_data, file)                

def generate_platforms(platform_generator_path, platforms_path, machines_percentage_usage, clusters_description_path):
    str(machines_percentage_usage)
    cmd = platform_generator_path #+ " " + platforms_path + platform_name + " "
    cmd += " -u " + "'" + str(machines_percentage_usage) + "'" + " -p " + str(platforms_path) + " -c " + str(clusters_description_path)
    print(cmd)
    os.system(cmd)

    return

def generate_workloads(profile_path, workload_generator_path,workloads_path, workload_sizes, platform_sizes, heterogeneity_levels, random_seeds): #optimization_factors, random_seeds):
    """
    ./workload_generator.py profiles_computation.json workload_computation_res_100_size_200_seed_15.json 100 -jn 200 -rs 15
    """

    print("--------------------- Generating the workloads ---------------------")

    for workload_size in workload_sizes:
        workload_filename = "workload_size_" + str(workload_size) + "_"
        for platform_size in platform_sizes:
            platform_string = "res_" + str(platform_size) + "_"
            for random_seed in random_seeds:
                seed_string = "seed_" + str(random_seed) + ".json"
                workload_name = workload_filename + platform_string + seed_string
                
                cmd = workload_generator_path + " " + profile_path + " " + workloads_path + workload_name
                cmd += " -rn " + str(platform_size) + " -jn " + str(workload_size) + " -rs " + str(random_seed)
                print(cmd)
                os.system(cmd)

                seed_string = ""
        platform_string = ""
    return    

def print_parameters():
    str = "\nPlease, indicate the yaml file with the parameters of the experiments, like \n\
    python3 generate_and_run_experiments.py parameters.yaml"
    print(str)
    return

def main():
    
    argvs = sys.argv
    if (len(argvs) == 1):
        print_parameters()
        return

    else:
        # Open the file and load the file
        with open(argvs[1]) as f:
            experiment_paramenters = yaml.load(f, Loader=SafeLoader)
        if(experiment_paramenters == None):
            print("Please, verify your input parameters file.")
            return
    
    exp_description_path = experiment_paramenters["exp_description_path"]
    exp_out_path = experiment_paramenters["exp_out_path"]
    scheduler_path = experiment_paramenters["scheduler_path"]
    profiles_description_per_machine_path = experiment_paramenters["profiles_description_per_machine_path"]
    container_description_path = experiment_paramenters["container_description_path"]
    batsim_flags = experiment_paramenters["batsim_flags"]
    environment_path = experiment_paramenters["environment_path"]

    scheduling_policies = experiment_paramenters["scheduling_policies"]
    platform_sizes = experiment_paramenters["platform_sizes"]
    workload_sizes = experiment_paramenters["workload_sizes"]
    heterogeneity_levels = experiment_paramenters["heterogeneity_levels"]
    random_seeds = experiment_paramenters["random_seeds"]
    machines_percentage_usage = experiment_paramenters["machines_percentage_usage"]

    platform_generator_path = experiment_paramenters["platform_generator_path"]
    platforms_path = experiment_paramenters["platforms_path"]

    workload_generator_path = experiment_paramenters["workload_generator_path"]
    workload_profile_path = experiment_paramenters["workload_profile_path"]
    workloads_path = experiment_paramenters["workloads_path"]
    container_description_path = experiment_paramenters["container_description_path"]
    clusters_description_path = experiment_paramenters["clusters_description_path"]

    batsim_failure_timeout = experiment_paramenters["failure_timeout"]
    batsim_ready_timout = experiment_paramenters["ready_timout"]
    batsim_simulation_timeout = experiment_paramenters["simulation_timeout"]
    batsim_success_timeout = experiment_paramenters["success_timeout"]

    #generate_platforms(platform_generator_path, platforms_path, machines_percentage_usage, clusters_description_path)

    cmd = 'ls ' + platforms_path
    platform_names = subprocess.getoutput(cmd).split("\n")
    print("List of platforms: ", platform_names)

    generate_workloads(workload_profile_path, workload_generator_path, workloads_path, workload_sizes, platform_sizes, heterogeneity_levels, random_seeds) #optimization_factors, )

    cmd = "ls " + workloads_path
    workload_names = subprocess.getoutput(cmd).split("\n")
    print("List of workloads: ", workload_names)

    generate_experiment_description(
        exp_description_path,
        exp_out_path,  
        scheduling_policies,
        scheduler_path, 
        platforms_path,
        platform_names,
        workloads_path, 
        workload_names,
        workload_sizes,
        random_seeds,
        profiles_description_per_machine_path,
        container_description_path,
        batsim_flags,
        batsim_failure_timeout,
        batsim_ready_timout,
        batsim_simulation_timeout,
        batsim_success_timeout)
    
main()

