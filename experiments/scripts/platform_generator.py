#!/usr/bin/env python3

import xml.etree.ElementTree as ET
import json
import argparse
import random

if __name__ == "__main__":

    # Program parameters parsing
    parser = argparse.ArgumentParser(
        description='Reads a JSON profiles description file and generates a '
                    'JSON workload (to be used by Batsim)')     
    parser.add_argument(
        '-u', '--machines_percentage_usage', type=str, default=None,
        help='The random seed to be used.')
    parser.add_argument(
        '-p', '--platform_path', type=str, default=None,
        help='The random seed to be used.')
    parser.add_argument(
        '-c', '--cluster_description_path', type=str, default=None,
        help='The random seed to be used.')
    args = parser.parse_args()
    args.machines_percentage_usage = eval(args.machines_percentage_usage)

    header = '''<?xml version='1.0'?>
    <!DOCTYPE platform SYSTEM "http://simgrid.gforge.inria.fr/simgrid/simgrid.dtd">\n'''
    
    sub_header = '''<platform version="4.1"> <zone id="AS0" routing="Full">\n'''
    footer = '''</zone> </platform>'''
    
    machine_header = '<host'
    machine_footer = '</host>'
    
    list_of_machines_percentage = args.machines_percentage_usage
    with open(args.cluster_description_path, "r") as file:
        input_json_data = json.load(file)

    for percentage in list_of_machines_percentage:
        number_total_of_machines = 0
        machine_id = 0
        machines_xml = '<host id="master_host" speed="1Mf"/>\n'
        for cluster in input_json_data.keys():
            number_of_machines = int(round(input_json_data[cluster]["number_of_machines"] * (percentage / 100), 0))
            number_total_of_machines += number_of_machines
            cpu_speed = input_json_data[cluster]["cpu_speed"]

            m = 0
            while m < number_of_machines:    
                machines_xml += machine_header + ' id="' + str(machine_id) + '"'
                machines_xml += ' speed="' + str(cpu_speed) + 'Mf">'
                machines_xml += '<prop id="speed" value="' + str(cpu_speed) + 'Mf"/>'
                machines_xml += '<prop id="cluster" value="' + str(cluster) + '"/>'
                machines_xml += machine_footer + '\n'
                machine_id += 1
                m += 1
    
        platform_name = "res" + "_" + str(number_total_of_machines) + "_" + \
                        "c" + "_" + str(len(input_json_data.keys())) + "_" + \
                        "p" + "_" + str(percentage) + ".xml"
        
        platform_xml = sub_header + machines_xml + footer

        tree = ET.ElementTree(ET.fromstring(platform_xml))
        with open(args.platform_path + platform_name, "wb") as f:
            f.write(header.encode('utf8'))
            tree.write(f, encoding='utf-8', xml_declaration=False)
