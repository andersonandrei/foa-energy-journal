#!/usr/bin/env python3

import argparse
import random
import json

if __name__ == "__main__":

    # Program parameters parsing
    parser = argparse.ArgumentParser(
        description='Reads a JSON profiles description file and generates a '
                    'JSON workload (used by Batsim) from it')
    parser.add_argument(
        'input_json', type=argparse.FileType('r'),
        help='The input JSON profiles description file')
    parser.add_argument(
        'output_json', type=argparse.FileType('w'),
        help='The output JSON workload file')
    parser.add_argument(
        '-rn', '--resources_number', type=int, default=100,
        help='The number of resources in the platform')
    parser.add_argument(
        '-min_duration', '--min_job_duration', type=int, default=60,
        help='The duration max in seconds of a job. Default of 60 seconds.')        
    parser.add_argument(
        '-min_cpu', '--min_cpu_usage', type=int, default=10,
        help='The max cpu usage of a job, in 1e6 flops') 
    parser.add_argument(
        '-max_duration', '--max_job_duration', type=int, default=18000,
        help='The duration max in seconds of a job. Default of 5 minutes, 18000 seconds.')        
    parser.add_argument(
        '-max_cpu', '--max_cpu_usage', type=int, default=10000,
        help='The max cpu usage of a job, in 1e6 flops')
    parser.add_argument(
        '-max_subtime', '--max_subtime', type=int, default=1000,
        help='Maximum submission time')
    parser.add_argument(
        '-max_walltime', '--max_walltime', type=int, default=18000,
        help='Maximum wall time')     
    parser.add_argument(
        '-rs', '--random_seed', type=int, default=None,
        help='The random seed')
    parser.add_argument(
        '-jn', '--job_number', type=int, default=300,
        help='The number of jobs to generate')

    args = parser.parse_args()

    if (args.random_seed != None):
        random.seed(args.random_seed)

    # Profile loading
    profiles = {}
    try:
        input_json_data = json.load(args.input_json)
        if "profiles" not in input_json_data:
            raise ValueError("No 'profiles' in {}".format(input_json_data))
        profiles = input_json_data["profiles"]
    except IOError:
        print('Cannot read file', args.input_json)
        raise

    # Creating a generic job object
    generic_job = {
        "id":0, 
        "subtime": 0, 
        #"walltime": 0, 
        "res": 1, 
        "profile": "profile_name"
    }

    # Create all jobs
    jobs = []
    job_id = 0
    list_of_profiles = list(input_json_data["profiles"].keys())
    while(job_id < args.job_number):
        new_job = generic_job.copy()
        new_job["id"] = job_id
        
        random_profile_id = random.randint(0, len(list_of_profiles) -1)
        random_profile = list_of_profiles[random_profile_id]
        new_job["profile"] = random_profile
        
        #random_duration = random.randint(args.min_job_duration, args.max_job_duration)
        #new_job["duration"] = random_duration
        
        #random_cpu_usage = random.randint(args.min_cpu, args.max_cpu)
        #new_job["cpu"] = random_cpu_usage

        #random_subtime = random.randint(0, args.max_subtime)
        #new_job["subtime"] = random_subtime

        #random_walltime = random.randint(0, args.max_walltime)
        #new_job["walltime"] = args.max_walltime

        jobs.append(new_job)
        job_id += 1

    # Create the final workload and export it as json
    workload = {
        "nb_res": args.resources_number,
        "jobs": jobs,
        "profiles": input_json_data["profiles"]
    }
    json.dump(workload, args.output_json,  indent=4)