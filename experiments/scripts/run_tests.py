import os
import subprocess
import csv
import yaml
from yaml.loader import SafeLoader
import sys
import glob

def print_parameters():
    str = "\nPlease, indicate the yaml file with the parameters of the analysis, like \n\
    python3 pre_process_outputs.py parameters.yaml"
    print(str)
    return

def main():

    argvs = sys.argv
    if (len(argvs) == 1):
        print_parameters()
        return

    else:
        with open(argvs[1]) as f:
            experiment_paramenters = yaml.load(f, Loader=SafeLoader)
        if(experiment_paramenters == None):
            print("Please, verify your input parameters file.")
            return

    current_results_path = experiment_paramenters["current_results_path"]
    unit_test_results_path = experiment_paramenters["unit_test_results_path"]
    paper_results_test_path = experiment_paramenters["paper_results_test_path"]
    scheduling_policies_to_test = experiment_paramenters["scheduling_policies_to_test"]
    metrics_to_test = experiment_paramenters["metrics_to_test"]
    test_id = experiment_paramenters["test_id"]

    missing_results = []
    failed_experiments = []
    for scheduling_policy in scheduling_policies_to_test:
        for metric in metrics_to_test:

            # The metric out_valid_solution is not applied to the KubernetesAlgo policy
            if (metric == "out_valid_solutions" and scheduling_policy == "kubernetesAlgo"):
                continue

            # We do not compare the results of out_schedule because it depends of the speed of 
            # the machine were the simulations were performed. Even so, the other metrics represent the same
            # results as this one (in another format)
            if (metric == "out_schedule"):
                continue

            current_results = [f for f in glob.glob(current_results_path + scheduling_policy + "/" + metric + "/*.csv")]
            list_current_results = [f.split("/")[-1] for f in current_results]

            unit_test_results = [f for f in glob.glob(unit_test_results_path + scheduling_policy + "/" + metric + "/*.csv")]
            list_unit_test_results = [f.split("/")[-1] for f in unit_test_results]

            paper_test_results = [f for f in glob.glob(paper_results_test_path + scheduling_policy + "/" + metric + "/*.csv")]
            list_paper_test_results = [f.split("/")[-1] for f in paper_test_results]    

            path_current_results = current_results_path + scheduling_policy + "/" + metric + "/"

            if(test_id == 0):
                list_tests_for_comparison = list_unit_test_results
                path_tests_for_comparison = unit_test_results_path + scheduling_policy + "/" + metric + "/"

            elif(test_id == 1):
                list_tests_for_comparison = list_paper_test_results
                path_tests_for_comparison = paper_results_test_path + scheduling_policy + "/" + metric + "/"

            else:
                print("Test ID not recognized.")
                return


            for result in list_current_results:
                if (result in list_tests_for_comparison):
                    cmd = "cmp " + path_current_results + result + " " + path_tests_for_comparison + result
                    comparison_result = subprocess.getoutput(cmd).split("\n")
                    if (comparison_result[0] != ''):
                        failed_experiments.append(path_current_results + result)
                else:
                    missing_results.append(path_current_results + result)

    if(len(failed_experiments) == 0 and len(missing_results) == 0):
        print("Success.")
    else:
        if (len(failed_experiments) != 0):
            print("The following experiments failed. The files differ from the comparison ones: ", failed_experiments)
        if (len(missing_results) != 0):
            print("The following experiments were not found in the comparison folder: ", missing_results)

main()