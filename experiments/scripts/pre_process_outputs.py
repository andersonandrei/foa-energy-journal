import os
import subprocess
import csv
import yaml
from yaml.loader import SafeLoader
import sys

def check_and_create_repositories(pattern_name, files_path, new_path):

    cmd = "ls " + new_path
    results_folders = subprocess.getoutput(cmd).split("\n") 
    if ("approxAlgoFirstFit" not in results_folders):
        cmd = "mkdir " + new_path + "approxAlgoFirstFit"
        os.system(cmd)
        cmd = "mkdir " + new_path + "approxAlgoFirstFit/" + pattern_name
        os.system(cmd)
    
    cmd = "ls " + new_path + "approxAlgoFirstFit"
    results_folders = subprocess.getoutput(cmd).split("\n")
    if (pattern_name not in results_folders):
        cmd = "mkdir " + new_path + "approxAlgoFirstFit/" + pattern_name
        os.system(cmd)

    cmd = "ls " + new_path
    results_folders = subprocess.getoutput(cmd).split("\n") 
    if ("approxAlgoSmallestFirst" not in results_folders):
        cmd = "mkdir " + new_path + "approxAlgoSmallestFirst"
        os.system(cmd)
        cmd = "mkdir " + new_path + "approxAlgoSmallestFirst/" + pattern_name
        os.system(cmd)
    
    cmd = "ls " + new_path + "approxAlgoSmallestFirst"
    results_folders = subprocess.getoutput(cmd).split("\n")
    if (pattern_name not in results_folders):
        cmd = "mkdir " + new_path + "approxAlgoSmallestFirst/" + pattern_name
        os.system(cmd)

    cmd = "ls " + new_path
    results_folders = subprocess.getoutput(cmd).split("\n") 
    if ("approxAlgoLargestFirst" not in results_folders):
        cmd = "mkdir " + new_path + "approxAlgoLargestFirst"
        os.system(cmd)
        cmd = "mkdir " + new_path + "approxAlgoLargestFirst/" + pattern_name
        os.system(cmd)
    
    cmd = "ls " + new_path + "approxAlgoLargestFirst"
    results_folders = subprocess.getoutput(cmd).split("\n")
    if (pattern_name not in results_folders):
        cmd = "mkdir " + new_path + "approxAlgoLargestFirst/" + pattern_name
        os.system(cmd)

    cmd = "ls " + new_path
    results_folders = subprocess.getoutput(cmd).split("\n") 
    if ("approxAlgoFCFS" not in results_folders):
        cmd = "mkdir " + new_path + "approxAlgoFCFS"
        os.system(cmd)
        cmd = "mkdir " + new_path + "approxAlgoFCFS/" + pattern_name
        os.system(cmd)
    
    cmd = "ls " + new_path + "approxAlgoFCFS"
    results_folders = subprocess.getoutput(cmd).split("\n")
    if (pattern_name not in results_folders):
        cmd = "mkdir " + new_path + "approxAlgoFCFS/" + pattern_name
        os.system(cmd)

    cmd = "ls " + new_path
    results_folders = subprocess.getoutput(cmd).split("\n") 
    if ("kubernetesAlgo" not in results_folders):
        cmd = "mkdir " + new_path + "kubernetesAlgo"
        os.system(cmd)
        cmd = "mkdir " + new_path + "kubernetesAlgo/" + pattern_name
        os.system(cmd)
    
    cmd = "ls " + new_path + "kubernetesAlgo"
    results_folders = subprocess.getoutput(cmd).split("\n")
    if (pattern_name not in results_folders):
        cmd = "mkdir " + new_path + "kubernetesAlgo/" + pattern_name
        os.system(cmd) 

    return

def move_files(pattern_name, files_path, new_path):
    
    print("\n------------------------------------ ")
    print("Moving: " + pattern_name + "\n")

    experiments_to_re_execute = []

    check_and_create_repositories(pattern_name, files_path, new_path)

    cmd = "ls " + files_path
    results_folders = subprocess.getoutput(cmd).split("\n")   
    for folder in results_folders:
        exp_failed = False

        #Check if it is null
        file_path = files_path + folder + "/"
        try:
            print(file_path + pattern_name + ".csv ")
            with open(file_path + "out_schedule.csv", 'r') as csvfile:
                csvreader = csv.reader(csvfile)
                for row in csvreader:
                    if row == []:
                        print("Failed workload")
                        experiments_to_re_execute.append(folder)
        except:
            exp_failed = True
            experiments_to_re_execute.append(folder)
        
        if (exp_failed == False):
            
            if ("approxAlgoFirstFit" in folder):
                cmd = "cp " + file_path + pattern_name + ".csv " + new_path + "approxAlgoFirstFit/" + pattern_name + "/" + folder + '.csv'
                print(cmd)

            if ("approxAlgoSmallestFirst" in folder):
                cmd = "cp " + file_path + pattern_name + ".csv " + new_path + "approxAlgoSmallestFirst/" + pattern_name + "/" + folder + '.csv'
                print(cmd)   

            if ("approxAlgoLargestFirst" in folder):
                cmd = "cp " + file_path + pattern_name + ".csv " + new_path + "approxAlgoLargestFirst/" + pattern_name + "/" + folder + '.csv'
                print(cmd)   
            
            if ("approxAlgoFCFS" in folder):
                cmd = "cp " + file_path + pattern_name + ".csv " + new_path + "approxAlgoFCFS/" + pattern_name + "/" + folder + '.csv'
                print(cmd)              

            if ('kubernetesAlgo' in folder and pattern_name != "out_valid_solutions"):
                cmd = "cp " + file_path + pattern_name + ".csv " + new_path + "kubernetesAlgo/" + pattern_name + "/" + folder + '.csv'
                print(cmd)

            os.system(cmd)
    
    return experiments_to_re_execute

def print_parameters():
    str = "\nPlease, indicate the yaml file with the parameters of the analysis, like \n\
    python3 pre_process_outputs.py parameters.yaml"
    print(str)
    return

def main():

    argvs = sys.argv
    if (len(argvs) == 1):
        print_parameters()
        return

    else:
        with open(argvs[1]) as f:
            experiment_paramenters = yaml.load(f, Loader=SafeLoader)
        if(experiment_paramenters == None):
            print("Please, verify your input parameters file.")
            return

    new_path = experiment_paramenters["analysis_results_path"]
    results_path = experiment_paramenters["analysis_exp_out_path"]

    cmd = "ls ../"
    results_folders = subprocess.getoutput(cmd).split("\n") 
    if ("results" not in results_folders):
        cmd = "mkdir " + new_path
        os.system(cmd)
    
    cmd = "ls ../analysis/"
    results_folders = subprocess.getoutput(cmd).split("\n") 
    if ("figures" not in results_folders):
        cmd = "mkdir figures"
        os.system(cmd)

    experiments_to_re_execute = move_files("out_schedule", results_path, new_path)
    move_files("out_download_data_info", results_path, new_path)
    move_files("out_valid_solutions", results_path, new_path)
    move_files("out_jobs", results_path, new_path)
    move_files("out_energy_consumption", results_path, new_path)

    print("\n----------------------------------------- ") 
    print("Failures detected in the following experiments: \n", experiments_to_re_execute)

main()