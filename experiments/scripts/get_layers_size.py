#!/usr/bin/env python3

import os
import subprocess
import json

if __name__ == "__main__":
    with open("../simulations/container_description_computation.json") as f:
       container_description = json.load(f)
    #print("Containers: ", container_description)

    for profile in container_description["profiles"]:
        print(profile)
        
        container_name = 'andersonandrei/' + profile.split("_")[0] + ":" + '_'.join(profile.split("_")[1:])
        print(container_name)
        cmd = "docker pull " + container_name
        os.system(cmd)
        
        layers = container_description["profiles"][profile].get("layers")
        if layers != None:
            for layer in layers.keys():
                print(layer)
                try:
                    cmd = "cat /var/lib/docker/image/overlay2/layerdb/sha256/" + layer + "/size"
                    #os.system(cmd)
                    size = subprocess.getoutput(cmd)#.split("\n")
                    print("Tamanho: ", size)
                except:
                    pass