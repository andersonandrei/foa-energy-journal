import os
import subprocess
import pandas as pd
import csv

def retrieve_iterations_processing_time(files_path):

    # Get the list of exp_out folders
    cmd = "ls " + files_path
    results_folders = subprocess.getoutput(cmd).split("\n")
    for folder in results_folders:
        print("Folder: ", folder)
        # To open the log file in the log folder

        try:
        #Check if it is null
            file_path = files_path + folder + "/log/sched.out.log"
            lp_iterations = 0
            curtime_iterations = []
            with open(file_path, 'r') as textfile:
                lines = textfile.readlines()
                for line in lines:
                    keyword = "(modified_LP)"
                    if keyword in line:
                        print("line", line)
                        curtime_iterations.append(line.split()[3]) # it provides the cumtime of cProfile result
                        lp_iterations += 1
                        print(lp_iterations, curtime_iterations)

            if (lp_iterations > 0):
                file_path = files_path + folder + "/out_approxAlgo_iterations_processing_time.csv"
                header = ["iterations", "processing_time"]
                with open(file_path, 'w', encoding='UTF8') as csvfile:
                    writer = csv.writer(csvfile)
                    writer.writerow(header)
                    for i in range(0, len(curtime_iterations)):
                        row = [i, curtime_iterations[i]]
                        writer.writerow(row)

        except:
            continue
        

def main():
    results_path = "../experiments/simulations/exp_out/"
    #new_path = "../results/"
    #experiments_to_re_execute = move_files("out_schedule", results_path, new_path)
    #move_files("out_download_data_info", results_path, new_path)
    #move_files("out_valid_solutions", results_path, new_path)
    retrieve_iterations_processing_time(results_path)

    print("----------------------------------------- ") 
    #print("Failures detected in the following experiments: \n", experiments_to_re_execute)

main()