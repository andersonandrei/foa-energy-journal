#!/bin/bash

EXP_DESCRIPTION_PATH="./simulations/exp_description/"

echo '===================================================='
echo 'Running the experiments'
echo '===================================================='
for entry in "$EXP_DESCRIPTION_PATH"*
do
  robin $entry
done