import os
import subprocess
import json
import yaml
from yaml.loader import SafeLoader
import sys

def run_experiments(exp_description_path, experiment_description_files, environment_path):
    """
    nix-shell ./tuto-env-pinned.nix --command 'robin ./expe_approxAlgo_computation_workload_1_platform_1.yaml'
    """

    print("--------------------- Running the experiments ---------------------")
    #general_command = "nix-shell " + environment_path + " --command 'robin "
    general_command = "robin "
    experiment_description_files_size = len(experiment_description_files)
    experiment_description_files_size_per_group = int(experiment_description_files_size / 3)
    print("Number of experiments: ", experiment_description_files_size)
    for experiment_id in range(0, experiment_description_files_size):
        experiment_description_file = experiment_description_files[experiment_id]
        print("================= ", experiment_description_file)
        cmd = general_command + exp_description_path + experiment_description_file #+ "'"
        print(cmd)
        os.system(cmd)

def print_parameters():
    str = "\nPlease, indicate the yaml file with the parameters of the experiments, like \n\
    python3 generate_and_run_experiments.py parameters.yaml"
    print(str)
    return

def main():
    
    argvs = sys.argv
    if (len(argvs) == 1):
        print_parameters()
        return

    else:
        # Open the file and load the file
        with open(argvs[1]) as f:
            experiment_paramenters = yaml.load(f, Loader=SafeLoader)
        if(experiment_paramenters == None):
            print("Please, verify your input parameters file.")
            return
    
    exp_description_path = experiment_paramenters["exp_description_path"]
    environment_path = experiment_paramenters["environment_path"]

    cmd = "ls " + exp_description_path
    experiment_description_files = subprocess.getoutput(cmd).split("\n")

    exp_files = {}
    exp_files["list_of_exp"] = experiment_description_files
    with open("experiments_to_be_executed.json", 'w') as file:
        json.dump(exp_files, file) 
    
    with open("experiments_to_be_executed.json", 'r') as file:
        json_file = json.load(file)
        experiment_description_files = json_file["list_of_exp"]
    print(experiment_description_files)
    
    run_experiments(exp_description_path, experiment_description_files, environment_path)
    
main()