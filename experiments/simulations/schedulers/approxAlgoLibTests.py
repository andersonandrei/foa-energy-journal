#class LP_Energy_Tests:
"""
    def __init__():
        print("-----------------------------------------------")
        print("Initializaing the tests.")
        print("-----------------------------------------------")
"""

def test_01():
    print("Test 01")
    
    N = list(range(1))
    H = list(range(1))
    K = list(range(1))

    c = [[1]]

    p = [[1]]

    c_tilde = [[1]]

    p_tilde = [[1]]

    env = [0]

    mc = [1] #number of machines per class

    Tmax = 10

    return N, H, K, c, p, c_tilde, p_tilde, mc, env, Tmax 

def test_02():
    print("Test 02")
    
    N = list(range(2))
    H = list(range(1))
    K = list(range(1))

    c = [[1,1]]

    p = [[1,1]]

    c_tilde = [[1]]

    p_tilde = [[1]]

    env = [0, 0]

    mc = [1] #number of machines per class

    Tmax = 10

    return N, H, K, c, p, c_tilde, p_tilde, mc, env, Tmax 

def test_03():
    print("Test 03")
    
    N = list(range(1))
    H = list(range(2))
    K = list(range(1))

    c = [[2],[1]]

    p = [[2],[1]]

    c_tilde = [[1],[1]]

    p_tilde = [[1],[1]]

    env = [0, 0]

    mc = [1, 1] #number of machines per class

    Tmax = 10

    return N, H, K, c, p, c_tilde, p_tilde, mc, env, Tmax 

def test_04():
    print("Test 04")
    
    N = list(range(1))
    H = list(range(2))
    K = list(range(1))

    c = [[1],[1]]

    p = [[1],[1]]

    c_tilde = [[1],[1]]

    p_tilde = [[1],[1]]

    env = [0, 0]

    mc = [1, 1] #number of machines per class

    Tmax = 10

    return N, H, K, c, p, c_tilde, p_tilde, mc, env, Tmax 

def test_05():
    print("Test 05")
    
    N = list(range(2))
    H = list(range(1))
    K = list(range(2))

    c = [[1,1]]

    p = [[1,1]]

    c_tilde = [[1,1]]

    p_tilde = [[1,1]]

    env = [0, 1]

    mc = [1] #number of machines per class

    Tmax = 10

    return N, H, K, c, p, c_tilde, p_tilde, mc, env, Tmax 

def test_06():
    print("Test 06")
    
    N = list(range(2))
    H = list(range(2))
    K = list(range(1))

    c = [[1, 1],
        [1, 1]]

    p = [[1, 1],
        [1, 1]]

    c_tilde = [[1, 1],
            [1, 1]]

    p_tilde = [[1, 1],
            [1, 1]]

    env = [0, 0]

    mc = [1, 1] #number of machines per class

    Tmax = 10

    return N, H, K, c, p, c_tilde, p_tilde, mc, env, Tmax 

def test_07():
    print("Test 07")
    
    N = list(range(2))
    H = list(range(2))
    K = list(range(1))

    c = [[10, 10],
        [1, 1]]

    p = [[10, 10],
        [1, 1]]

    c_tilde = [[1, 1],
            [1, 1]]

    p_tilde = [[1, 1],
            [1, 1]]

    env = [0, 0]

    mc = [1, 1] #number of machines per class

    Tmax = 10

    return N, H, K, c, p, c_tilde, p_tilde, mc, env, Tmax 

def test_08():
    print("Test 08")
    
    N = list(range(2))
    H = list(range(2))
    K = list(range(2))

    c = [[10, 1],
        [1, 10]]

    p = [[10, 1],
        [1, 10]]

    c_tilde = [[1, 1],
            [1, 1]]

    p_tilde = [[1, 1],
            [1, 1]]

    env = [0, 0]

    mc = [1, 1] #number of machines per class

    Tmax = 12

    return N, H, K, c, p, c_tilde, p_tilde, mc, env, Tmax 

def test_09():
    print("Test 09")

    N = list(range(2))
    H = list(range(2))
    K = list(range(2))

    c = [[1, 1],
        [1, 1]]

    p = [[1, 1],
        [1, 1]]

    c_tilde = [[1, 1],
            [1, 1]]

    p_tilde = [[1, 1],
            [1, 1]]

    env = [0, 0]

    mc = [1, 1] #number of machines per class

    Tmax = 12

    return N, H, K, c, p, c_tilde, p_tilde, mc, env, Tmax 

def test_10():
    print("Test 10")

    N = list(range(2))
    H = list(range(2))
    K = list(range(2))

    c = [[1, 1],
        [1, 1]]

    p = [[1, 1],
        [1, 1]]

    c_tilde = [[1, 10],
            [10, 10]]

    p_tilde = [[1, 1],
            [1, 10]]

    env = [0, 1]

    mc = [1, 1] #number of machines per class

    Tmax = 12

    return N, H, K, c, p, c_tilde, p_tilde, mc, env, Tmax 

def test_11():
    print("Test 11")
    
    N = list(range(3))
    H = list(range(2))
    K = list(range(3))

    c = [[1, 1, 1],
        [1, 1, 1]]

    p = [[1, 1, 1],
        [1, 1, 1]]

    c_tilde = [[1, 10, 1],
            [1, 1, 1]]

    p_tilde = [[1, 1, 1],
            [1, 1, 1]]

    env = [1, 2, 0]

    mc = [1, 1] #number of machines per class

    Tmax = 12

    return N, H, K, c, p, c_tilde, p_tilde, mc, env, Tmax 

def test_12():
    print("Test 12")
    
    N = list(range(3))
    H = list(range(2))
    K = list(range(2))

    c = [[1, 1, 1], [1, 1, 1]]

    p = [[1, 1, 1], [1, 1, 1]]

    c_tilde = [[1, 1, 1], [1, 1, 1]]

    p_tilde = [[1, 1, 1], [1, 1, 1]]

    mc = [1, 2]

    env = [0, 0, 1]

    Tmax = 6

    return N, H, K, c, p, c_tilde, p_tilde, mc, env, Tmax 

def test_13():
    print("Test 13")
    N = list(range(7))
    H = list(range(3))
    K = list(range(1))

    c =    [[3, 1, 1, 1, 1, 1, 1],
            [3, 1, 1, 1, 1, 1, 1],
            [3, 1, 1, 1, 1, 1, 1]]

    p =    [[3, 1, 1, 1, 1, 1, 1],
            [3, 1, 1, 1, 1, 1, 1],
            [3, 1, 1, 1, 1, 1, 1]]

    c_tilde =   [[0], 
                [0], 
                [0]]

    p_tilde =   [[0], 
                [0], 
                [0]]

    mc = [1, 1, 1]

    env =  [0, 0, 0, 0, 0, 0, 0]

    Tmax = 3
    return N, H, K, c, p, c_tilde, p_tilde, mc, env, Tmax

def test_14():
    print("Test 14")
    N = list(range(7))
    H = list(range(3))
    K = list(range(1))

    c =    [[3, 1, 1, 1, 1, 1, 1],
            [3, 1, 1, 1, 1, 1, 1],
            [3, 1, 1, 1, 1, 1, 1]]

    p =    [[3, 1, 1, 1, 1, 1, 1],
            [3, 1, 1, 1, 1, 1, 1],
            [3, 1, 1, 1, 1, 1, 1]]

    c_tilde =   [[1], 
                [2], 
                [0]]

    p_tilde =   [[1], 
                [2], 
                [0]]

    mc = [1, 1, 1]

    env =  [0, 0, 0, 0, 0, 0, 0]

    Tmax = 7
    return N, H, K, c, p, c_tilde, p_tilde, mc, env, Tmax        