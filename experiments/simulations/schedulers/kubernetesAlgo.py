from pickle import FALSE
from batsim.batsim import BatsimScheduler, Batsim

import csv
import json
import math
from procset import ProcSet
from itertools import islice


class KubernetesAlgo(BatsimScheduler):

    def __init__(self, options):
        super().__init__(options)

        # Verify if the input_path was provided
        #assert "container_description_path" in options, "The path to the input files should be given as a CLI option as follows: [pybatsim command] -o \'{\"input_path\":\"path/to/input/files\"}\'"
        #if not os.path.exists(options["container_description_path"]):
        if "container_description_path" not in options:
           assert False, "Could not find input path {}".format(options["container_description_path"])

        if "profiles_description_per_machine_path" not in options:
           assert False, "Could not find input path {}".format(options["profile_description_per_machine_path"])                

        if "download_info_csv_path" in options:
            self.download_info_csv_path = options["download_info_csv_path"]
        else:
            assert False, "Could not find input path {}".format(options["download_info_csv_path"])

        if "workload_size" in options:
            self.workload_size = options["workload_size"]
        else:
            assert False, "Could not find input path {}".format(options["workload_size"])

        if "random_seed" in options:
            self.random_seed = options["random_seed"]
        else:
            assert False, "Could not find input path {}".format(options["random_seed"])

        # Read and save the external profiles (for containers)
        self.list_of_containers = []
        with open(options["container_description_path"]) as f:
            self.container_description = json.load(f)
        for container in self.container_description["profiles"].keys():
            self.list_of_containers.append(container)

        with open(options["profiles_description_per_machine_path"]) as f:
            self.profile_computation_description = json.load(f)            

        self.nb_completed_jobs = 0
        self.nb_jobs = 0
        self.nb_container_downloaded = 0
        self.total_container_downloaded_mb = 0
        self.total_container_downloaded_mb_expected = 0
        self.total_io_mb = 0
        self.total_energy_consumption = 0
        self.platform_total_energy_consumption = 0        

        self.notify_already_sent = False
        self.end_of_simulation_asked = False
        self.start_simulation = False
        self.global_allocation_done = False

        self.time_next_update = 1.0
        self.update_period = 150
        self.sched_delay = 0.005

        self.jobs_completed = []
        self.jobs_waiting = []
        self.required_containers = []
        self.mapping_job_container = {}
        self.mapping_machine_container = {}
        self.mapping_original_job_updated_job = {}
        self.mapping_energy_consumption_per_job = {}

        self.list_of_machines = []
        self.machines_description = {}
        self.machines_per_cluster = []
        self.cluster_description = {}
        self.cluster_allocation_dict = {}
        self.cluster_allocation_dict_original = {}

        self.scheduling_mapping = {}
        self.availableResources = None
        self.availableResourcesPerCluster = {}
        self.openJobs = set()

        self.global_scheduledJobs = []

        self.container_jobs_scheduled = []
        self.container_jobs_executed = []
        self.original_jobs_scheduled = []

    def save_output_as_csv(self, file_name, json_data):
        header = []
        data = []    

        for key,value in json_data.items():
            header.append(key)
            data.append(value)

        with open(file_name, 'w', encoding='UTF8') as f:
            writer = csv.writer(f)

            # write the header
            writer.writerow(header)
            # write the data
            writer.writerow(data)
        return

    def downloading_container_as_job(self, job):
        """
        Check if the job is a dynamic job representing a container being downloaded.
        """

        is_container = [s for s in self.list_of_containers if s in job.id]
        if (len(is_container) != 0):
            return is_container[0]
        else:
            return None

    def get_layers_from_container(self, job_container):
        """
        Get the layers of a job_cotainer and return as a list.
        """
        list_of_layers = self.container_description.get("profiles").get(job_container).get("layers")
        if(list_of_layers != None):
            return list_of_layers
        else:
            return {}

    def get_layers_total_download_size_from_container(self, job_container):
        """
        Get the layers of a job_cotainer and return as a list.
        """

        total_download_size = self.container_description.get("profiles").get(job_container).get("size")
        if(total_download_size != None):
            return total_download_size
        else:
            return -1

    def list_machines_with_container(self, cluster_id, job_container, check_layers=False):
        """
        List machines that already have job_container
        """

        machine_candidates = []
        scores_machine_container = {}
        scores_machine_container_for_deployment = {}
        
        if (len(self.availableResourcesPerCluster[cluster_id]) == 0):
            return [], {}
        else:
            # Let's check if any machine available has the job_container
            # or other container with common layers
            for machine in self.availableResourcesPerCluster[cluster_id]:
                machine_id = int(str(machine))
                scores_machine_container[machine_id] = 0
                scores_machine_container_for_deployment[machine_id] = 0
                containers_in_machine = self.mapping_machine_container[machine_id]
                # It has the job_container
                if (job_container in containers_in_machine):
                    scores_machine_container[machine_id] = 1 #self.get_layers_total_download_size_from_container(job_container)
                    scores_machine_container_for_deployment[machine_id] = 1
                    #machine_candidates.append(machine_id)
                else:
                    scores_machine_container[machine_id] = 0 #self.get_layers_total_download_size_from_container(job_container)
                    scores_machine_container_for_deployment[machine_id] = 0
                    #machine_candidates.append(machine_id)


                # Verification for Deployment 
                # Check other container layers, and compute the percetage of matching
                if(check_layers and job_container not in containers_in_machine):
                    list_of_layers = self.get_layers_from_container(job_container)
                    for container in containers_in_machine:
                        
                        list_of_layers_of_second_container = self.get_layers_from_container(container)
                        for layer in list_of_layers:
                            if layer in list_of_layers_of_second_container:
                                scores_machine_container_for_deployment[machine_id] += list_of_layers_of_second_container.get(layer)
   
                    layers_total_download_size = self.get_layers_total_download_size_from_container(job_container)
                    total_number_of_layers_per_container = len(self.get_layers_from_container(job_container))

                    if(scores_machine_container_for_deployment[machine_id] != 0 and layers_total_download_size != -1):
                        scores_machine_container_for_deployment[machine_id] /= total_number_of_layers_per_container
                    
                    # If there is any problem with the container definition, some missing size in the .json file, for example, consider such container as invalid, so size 0
                    else:
                        scores_machine_container_for_deployment[machine_id] = 0
                
            machine_candidates = sorted(scores_machine_container, key=scores_machine_container.get, reverse=True)
                
            return machine_candidates, scores_machine_container_for_deployment
        
    def list_machines_with_container_layers(self, cluster_id, job_container, check_layers=False):
        """
        List machines that already have job_container
        """

        machine_candidates = []
        scores_machine_container = {}
        scores_machine_container_for_deployment = {}
        
        if (len(self.availableResourcesPerCluster[cluster_id]) == 0):
            return [], {}
        else:
            # Let's check if any machine available has the job_container
            # or other container with common layers
            for machine in self.availableResourcesPerCluster[cluster_id]:
                machine_id = int(str(machine))
                scores_machine_container[machine_id] = 0
                scores_machine_container_for_deployment[machine_id] = 0
                containers_in_machine = self.mapping_machine_container[machine_id]
                # It has the job_container
                if (job_container in containers_in_machine):
                    scores_machine_container[machine_id] = 1 #self.get_layers_total_download_size_from_container(job_container)
                    scores_machine_container_for_deployment[machine_id] = 1
                    machine_candidates.append(machine_id)
                
                # Verification for Deployment 
                # Check other container layers, and compute the percetage of matching
                if(check_layers and job_container not in containers_in_machine):
                    list_of_layers = self.get_layers_from_container(job_container)
                    for container in containers_in_machine:
                        
                        list_of_layers_of_second_container = self.get_layers_from_container(container)
                        for layer in list_of_layers:
                            if layer in list_of_layers_of_second_container:
                                scores_machine_container_for_deployment[machine_id] += list_of_layers_of_second_container.get(layer)
   
                    layers_total_download_size = self.get_layers_total_download_size_from_container(job_container)
                    total_number_of_layers_per_container = len(self.get_layers_from_container(job_container))

                    if(scores_machine_container_for_deployment[machine_id] != 0 and layers_total_download_size != -1):
                        scores_machine_container_for_deployment[machine_id] /= total_number_of_layers_per_container
                    
                    # If there is any problem with the container definition, some missing size in the .json file, for example, consider such container as invalid, so size 0
                    else:
                        scores_machine_container_for_deployment[machine_id] = 0
                
            machine_candidates = sorted(scores_machine_container, key=scores_machine_container.get, reverse=True)
                
            return machine_candidates, scores_machine_container_for_deployment
        

    def get_earliest_submitted_job(self):
        selected_job = None
        selected_job_id = None
        for job in self.openJobs:
            job_id = int(job.id.split("!")[1])
            if (selected_job == None):
                selected_job = job
            elif (selected_job_id > job_id):
                selected_job = job
            selected_job_id = int(selected_job.id.split("!")[1])
        return selected_job

    def onSimulationBegins(self):
        """
        Verify if the correct flags has been set when the simulation begins
        """

        assert self.bs.dynamic_job_registration_enabled, "Registration of dynamic jobs must be enabled for this scheduler to work"
        assert self.bs.ack_of_dynamic_jobs == False, "Acknowledgment of dynamic jobs must be disabled for this scheduler to work"
        
        self.bs.register_profiles("w0", self.container_description["profiles"])
        self.bs.register_profiles("w0", self.profile_computation_description["profiles"])

    def onAfterBatsimInit(self):
        """
        Update the set of Jobs and Resources after the simulation begins
        """

        self.openJobs = set()
        self.availableResources = ProcSet((0,self.bs.nb_compute_resources-1))
        for availableResource in self.availableResources:
            self.mapping_machine_container[availableResource] = []
        self.get_machines_and_speed(self.bs.machines["compute"])
        self.get_machines(self.bs.machines["compute"])
        self.get_machines_per_cluster(self.bs.machines["compute"])            

    def onBeforeEvents(self):
        """
        Update Batsim time with some small delay before new events happen.
        """

        if self.bs.time() >= self.time_next_update:
            self.time_next_update = math.floor(self.bs.time()) + self.update_period

    def convertLPSolutionToBatsimFormat(self, lp_solution):

        allocation_dict = {}
        for cluster_id in range(0, len(lp_solution)):
            for job_id in range(0, len(lp_solution[cluster_id])):
                if (lp_solution[cluster_id][job_id] == 1):
                    cluster = self.mapping_machine_id[cluster_id]
                    job = self.mapping_job_id[job_id]
                    if(allocation_dict.get(cluster) == None):
                        allocation_dict[cluster] = [job]
                    else:
                        allocation_dict[cluster].append(job)

        return allocation_dict
    
    def global_first_fit(self):
        # To inialize the allocation dict with empty lists
        cluster_allocation_dict = {}
        total_number_of_clusters = len(self.cluster_description)
        for i in range(total_number_of_clusters):
            cluster_allocation_dict[i] = []

        cluster_id = 0
        processed_jobs = []
        number_of_clusters = len(self.cluster_description)
        initial_cluster_id = cluster_id

        for new_job in self.openJobs:
            processed_jobs.append(new_job)
            cluster_allocation_dict[cluster_id].append(new_job)
            cluster_id += 1
            if (cluster_id == number_of_clusters):
                cluster_id = initial_cluster_id
        
        print("Gloabl FCFS solution: ", cluster_allocation_dict)
        return cluster_allocation_dict    

    def local_first_fit(self, cluster_allocation_dict):
        # To inialize the allocation dict with empty lists
        allocation_dict = {}
        total_number_of_machines = len(self.list_of_machines)
        for i in range(total_number_of_machines):
            allocation_dict[i] = []

        machine_id = 0
        max_machine_id = 0
        for cluster in cluster_allocation_dict:
            temporary_allocation_queue = cluster_allocation_dict[cluster]
            number_of_machines = self.cluster_description.get(cluster).get("number_of_machines")
            initial_machine_id = machine_id
            max_machine_id += number_of_machines
            while len(temporary_allocation_queue) > 0:
                new_job = temporary_allocation_queue.pop(0)
                allocation_dict[machine_id].append(new_job)
                if (machine_id < max_machine_id - 1):
                    machine_id += 1
                else:
                    machine_id = initial_machine_id
            machine_id = max_machine_id
        print("Local FCFS solution: ", allocation_dict)
        return allocation_dict    

    def best_fit(self, lp_solution_per_cluster, time_limit):
    #def best_fit(self, cluster_allocation_dict):
        # To inialize the allocation dict with empty lists
        allocation_dict = {}
        total_number_of_machines = len(self.list_of_machines)
        for i in range(total_number_of_machines):
            allocation_dict[i] = {}
            allocation_dict[i]["processing_volume"] = 0.0
            allocation_dict[i]["allocation"] = []

        for cluster in lp_solution_per_cluster:
            temporary_allocation_queue = self.sort_jobs_by_size(lp_solution_per_cluster[cluster], cluster)
            if (len(temporary_allocation_queue) == 0): 
                continue
            
            cluster_machines_cpu_speed = self.cluster_description[cluster]["speed"].split("Mf")[0]
            machines_per_cluster = self.cluster_description.get(cluster).get("list_of_machines_id")#.sort()

            machines_per_cluster_id = 0
            machine_id = machines_per_cluster[machines_per_cluster_id]
            machine_name_cluster = self.cluster_description[cluster]["cluster"]

            while(len(temporary_allocation_queue) != 0 and machines_per_cluster_id < len(machines_per_cluster)):
                job_per_machine_name = temporary_allocation_queue[0].profile + "_" + machine_name_cluster
                function_computation_needed = float(self.profile_computation_description["profiles"][job_per_machine_name].get("cpu"))
                job_size = round((function_computation_needed / float(cluster_machines_cpu_speed)) / 1000000, 2)

                while (allocation_dict[machine_id].get("processing_volume") + job_size < time_limit * 2):

                    new_job = temporary_allocation_queue.pop(0)
                    allocation_dict[machine_id]["allocation"].append(new_job.id)
                    allocation_dict[machine_id]["processing_volume"] += job_size

                    if (len(temporary_allocation_queue) == 0):
                        break

                    job_per_machine_name = temporary_allocation_queue[0].profile + "_" + machine_name_cluster
                    function_computation_needed = float(self.profile_computation_description["profiles"][job_per_machine_name].get("cpu"))
                    job_size = round((function_computation_needed / float(cluster_machines_cpu_speed)) / 1000000, 2)

                machines_per_cluster_id += 1
                machine_id = machines_per_cluster[machines_per_cluster_id] 

        return allocation_dict    

    def scheduleJobs(self):
        """
        The decion process. It will check if the machines have containers required by the jobs.
        If not, dybamic jobs will be created, and these jobs will represent the downloading of containers.
        """

        if(len(self.openJobs) == self.workload_size):
            self.start_simulation = True

            # Call global continuum first fit
            if (len(self.openJobs) > 0 and self.global_allocation_done == False):
                self.cluster_allocation_dict = self.global_first_fit()
                self.cluster_allocation_dict_original = self.global_first_fit()
                self.global_allocation_done = True

        scheduledJobs = []
        for cluster_id in self.cluster_description:
            if (self.global_allocation_done ==  False):
                break

            if len(self.availableResourcesPerCluster[cluster_id]) == 0:
                continue

            temporary_queue_of_jobs_per_cluster = self.cluster_allocation_dict[cluster_id]
            total_machines_per_cluster_id = 0
            machines_used = []
            machine_name_cluster = self.cluster_description[cluster_id]["cluster"]
                
            platform_profile_as_function = "platform_" + machine_name_cluster + "_worker"
            energy_per_machine_on_cluster = float(self.profile_computation_description["profiles"][platform_profile_as_function].get("energy_consumption")) 
            while(len(self.availableResourcesPerCluster[cluster_id]) > 0 and len(temporary_queue_of_jobs_per_cluster) > 0):
                job = temporary_queue_of_jobs_per_cluster.pop(0)
                job_io_size = job.profile_dict["io"]
                job_container = job.profile_dict['container']['image'] + "_"  + job.profile_dict['container']['tag']
                job_container_size = self.container_description["profiles"][job_container]["size"]
                #machine_cluster = self.cluster_description[cluster_id]["cluster"]
            
                # If it is a new container, save its name
                if (job_container not in self.required_containers):
                    self.required_containers.append(job_container)           
                
                # Search the best machine available, which means, one with the required container
                download_reduction = 0
                check_layers = True
                machine_candidates, scores_machine_container = self.list_machines_with_container(cluster_id, job_container, check_layers)
                if (len(machine_candidates) != 0):
                    machine = machine_candidates[0]
                    if (machine not in machines_used):
                        machines_used.append(machine)
                    download_reduction = scores_machine_container[machine]
                    machine = ProcSet((machine,machine)) # Convert the machine id to a ProcSet
                else:
                    break


                
                # Compute the energy consumption
                machine_cpu_speed = self.cluster_description[cluster_id]["speed"].split("Mf")[0]
                job_per_machine_name = job.profile + "_" + machine_name_cluster
                function_computation_needed = float(self.profile_computation_description["profiles"][job_per_machine_name].get("cpu"))
                function_execution_time = round((function_computation_needed / float(machine_cpu_speed)) / 1000000, 2)
                if (function_execution_time <= 0 ):
                    function_execution_time = 1
                energy_consumption = float(self.profile_computation_description["profiles"][job_per_machine_name].get("energy_consumption")) #* function_execution_time

                job.profile_dict['cpu'] = function_computation_needed
                job.profile_dict['energy_consumption'] = energy_consumption
                job_energy_consumption = energy_consumption

                new_profile_name = job.profile + "_" + str(machine_name_cluster)

                new_profile = {}
                #if new_profile_name not in self.container_description["profiles"].keys():
                if new_profile_name not in list(self.profile_computation_description['profiles'].keys()):
                    new_profile[new_profile_name] = self.container_description["profiles"].get(job_container)
                    
                    new_profile[new_profile_name]["cpu"] = job.profile_dict["cpu"]
                    new_profile[new_profile_name]["energy_consumption"] = job.profile_dict["energy_consumption"]

                    self.container_description["profiles"][new_profile_name] = new_profile
                    self.bs.register_profiles("w0", new_profile)
                
                # Create a dynamic job
                updated_job = self.bs.register_job(
                        job.id + "_updated", #job.workload + '!' + job_container + "_job" + str(job.id.split("!")[1]) + "_" + str(self.nb_container_downloaded),
                        1, 
                        2000,
                        new_profile_name, 
                        subtime=None)

                # If the container is not on the machine, download it there, before scheduling the job
                if (job_container != None and 
                    job_container not in self.mapping_machine_container[int(str(machine))]):
                    self.total_container_downloaded_mb_expected += job_container_size
                    new_profile_name = job_container
                    new_size = job_container_size

                    # If there are usefull layers in the allocated machine, create a new profile for such job, with a new delay
                    if (download_reduction != 0):
                        new_profile_name = job_container + '_reduced_' + str(round(download_reduction, 2))
                        new_profile = {}
                        if new_profile_name not in self.container_description["profiles"].keys():
                            new_profile[new_profile_name] = self.container_description["profiles"].get(job_container)
                            
                            if (download_reduction >= 1):
                                new_computation_required = 1
                                new_size = 1
                                
                            else:
                                new_computation_required = round(new_profile[new_profile_name]["cpu"] - (new_profile[new_profile_name]["cpu"] * download_reduction), 2)
                                new_size = round(new_profile[new_profile_name]["size"] - (new_profile[new_profile_name]["size"] * download_reduction), 2)

                            new_profile[new_profile_name]["cpu"] = new_computation_required
                            new_profile[new_profile_name]["size"] = new_size
                            
                            self.container_description["profiles"][new_profile_name] = new_profile
                            self.bs.register_profiles("w0", new_profile)

                    # Create a dynamic job
                    new_job = self.bs.register_job(
                        job.workload + '!' + job_container + "_job" + str(job.id.split("!")[1]) + "_" + str(self.nb_container_downloaded),
                        1, 
                        2000,
                        new_profile_name, 
                        subtime=None)
                    
                    self.container_jobs_scheduled.append(new_job)
                    self.total_container_downloaded_mb += new_size

                    # Allocate the new job to the machine reserved, and add it in the scheduledJobs list
                    new_job.allocation = machine
                    scheduledJobs.append(new_job)

                    # Save where job should be executed, and what is the container it depends on
                    job.allocation = machine
                    updated_job.allocation = machine
                    self.mapping_original_job_updated_job[job.id] = [job, updated_job.id]
                    self.mapping_job_container[updated_job.id] = [updated_job, new_job.id]
                    
                    self.nb_jobs += 2
                    self.nb_container_downloaded += 1

                    # Add the container in the machine
                    self.mapping_machine_container[int(str(machine))].append(job_container)

                # Or the container is already in the machine, or the job does not require a container, 
                # so the job can be scheduled
                else:
                    self.global_scheduledJobs.append(job)

                    # Allocate the new job to the machine reserved, and add it in the scheduledJobs list
                    job.allocation = machine
                    updated_job.allocation = machine
                    #scheduledJobs.append(job)
                    scheduledJobs.append(updated_job)
                    self.mapping_original_job_updated_job[job.id] = [job, updated_job.id]
                    self.nb_jobs += 1
                print("Job, energy consumption", job.id, job_energy_consumption)
                self.total_io_mb += job_io_size
                self.total_energy_consumption += job_energy_consumption
                self.availableResources -= machine
                self.availableResourcesPerCluster[cluster_id] -= machine
                self.openJobs.remove(job)

                # If all jobs were processed, break the loop to avoid iterating in the first loop for nothing
                # If there is no more resources available at all (in all clusters) stop it for a while
                #if(len(self.availableResources) == 0):
                    #if(len(self.openJobs) == 0):                
                    #print("Available resources over at all")
                    #break

                """
                if(len(self.availableResourcesPerCluster[cluster_id]) == 0 or len(temporary_queue_of_jobs_per_cluster) == 0):
                    if (cluster_id < len(self.cluster_description) - 1):
                        cluster_id += 1
                    else:
                        cluster_id = 0
                    continue
                """
        
            total_machines_per_cluster_id = len(machines_used)
            self.platform_total_energy_consumption += total_machines_per_cluster_id * energy_per_machine_on_cluster
            print("platform_profile_as_function", platform_profile_as_function)
            print("energy_per_machine_on_cluster: ", energy_per_machine_on_cluster)
            print("Machines used: ", machines_used)
            print("In cluster, we used: ", total_machines_per_cluster_id)
            print("Energy used here: ", total_machines_per_cluster_id * energy_per_machine_on_cluster)

        # Update time
        self.bs.consume_time(self.sched_delay)

        # Send the scheduled jobs to Batsim
        if len(scheduledJobs) > 0:
            self.bs.execute_jobs(scheduledJobs)

    def onJobSubmission(self, job):
        if (self.downloading_container_as_job(job) == None):
            self.openJobs.add(job)
        self.scheduleJobs()
    

    def onJobCompletion(self, job):
        self.nb_completed_jobs += 1
        self.jobs_completed.append(job)
        machine_id = int(str(job.allocation))

        # If the completed job is a dynamic job (container), we will use the same machine to compute the original job
        # that required such dynamic job.
        container_name = self.downloading_container_as_job(job)
        if (container_name != None or ("_" in job.id and job.id.split("_")[1] == 'updated')):

            job_related_to_dynamic_job = None
            if (container_name != None):
                # Add the container in the machine
                if(container_name not in self.mapping_machine_container[machine_id]):
                    self.mapping_machine_container[machine_id].append(container_name)
                
                # Iterate over the mapping_job_container to find the job related to this dynamic job
                for map_job_container in self.mapping_job_container.items():
                    if(map_job_container[1][1] == job.id):
                        job_related_to_dynamic_job = map_job_container[1][0]

            else:
                # Iterate over the mapping_job_container to find the job related to this dynamic job
                for map_original_job_updated_job in self.mapping_original_job_updated_job.items():
                    if(map_original_job_updated_job[1][1] == job.id):
                        job_related_to_dynamic_job = map_original_job_updated_job[1][0]

            # If some job is found, send it to be executed.
            if (job_related_to_dynamic_job != None):
                scheduledJobs = [job_related_to_dynamic_job]
                #print("Achou job pra esse container, mandou executar: ", scheduledJobs)
                self.bs.execute_jobs(scheduledJobs)
        
        # If it was an original job that was completed, we need to free the machine used
        else:
            # Free the mapping_job_container to not waste memory
            if self.mapping_job_container.get(job.id) != None:
                del self.mapping_job_container[job.id]

            # Free resources (machines)
            job_cluster_id = None
            for cluster_id in self.cluster_allocation_dict:
                if job in self.cluster_allocation_dict_original[cluster_id]:
                    job_cluster_id = cluster_id
                    break

            if (len(self.availableResources) == 0):
                self.availableResources = job.allocation
                self.availableResourcesPerCluster[job_cluster_id] = job.allocation
            else:
                self.availableResources |= job.allocation 
                self.availableResourcesPerCluster[job_cluster_id] |= job.allocation 

        if(len(self.openJobs) > 0 and len(self.availableResources) > 0):
            self.scheduleJobs()         

    def onNoMoreEvents(self):
        if(self.bs.nb_jobs_submitted != 0 and len(self.openJobs) == 0 and len(self.jobs_completed) == self.bs.nb_jobs_submitted and self.notify_already_sent == False):
            self.notify_already_sent = True
            self.bs.notify_registration_finished()

            output_data = {
                "total_io": self.total_io_mb,
                "total_container_data_downloaded_mb_expected": self.total_container_downloaded_mb_expected,
                "total_container_data_downloaded_mb": self.total_container_downloaded_mb, 
                "nb_different_required_containers": len(self.required_containers),
                "nb_container_downloaded": self.nb_container_downloaded,
                "total_io_and_container_data_downloaded": self.total_io_mb + self.total_container_downloaded_mb
            }
            self.save_output_as_csv(self.download_info_csv_path + "out_download_data_info.csv", output_data)

            energy_consumption_info_csv_path = self.download_info_csv_path
            output_data_energy = {
                "output_data_energy": self.total_energy_consumption, #round(self.total_energy_consumption * 1000, 2), # converting to Joules
                "platform_output_data_energy": self.platform_total_energy_consumption,
                "total_output_data_energy": self.platform_total_energy_consumption + self.total_energy_consumption,
            }
            self.save_output_as_csv(energy_consumption_info_csv_path + "out_energy_consumption.csv", output_data_energy)

    def get_machines_and_speed(self, machines_resource_description):
        for machine in machines_resource_description:
            self.machines_description[machine["id"]] = machine["properties"]["speed"]

    def get_machines(self, machines_resource_description):
        for machine in machines_resource_description:
            self.list_of_machines.append(machine["id"])

    def get_machines_per_cluster(self, machines_resource_description):
        list_of_machines_speed = [machine.get("properties").get("speed") for machine in machines_resource_description]
        number_of_machines_per_speed=dict(zip(list(list_of_machines_speed),[list(list_of_machines_speed).count(i) for i in list(list_of_machines_speed)]))

        cluster_id = 0
        init_range = 0 
        for machine_speed in number_of_machines_per_speed.keys():
            max_range = number_of_machines_per_speed[machine_speed]
            self.cluster_description[cluster_id] = {}
            self.cluster_description[cluster_id]["speed"] = machine_speed
            self.cluster_description[cluster_id]["number_of_machines"] = number_of_machines_per_speed[machine_speed]
            self.cluster_description[cluster_id]["list_of_machines_id"] = list(range(init_range, init_range + max_range))

            self.availableResourcesPerCluster[cluster_id] = ProcSet((init_range, init_range + max_range - 1))

            cluster_id += 1
            init_range += max_range

        self.machines_per_cluster = [machine.get("number_of_machines") for machine in self.cluster_description.values()]

        for cluster in self.cluster_description:
            cpu_speed = self.cluster_description[cluster].get("speed")
            for machine in machines_resource_description:
                if (machine.get("properties").get("speed") == cpu_speed):
                    self.cluster_description[cluster]["cluster"] = machine.get("properties").get("cluster")
                    break
