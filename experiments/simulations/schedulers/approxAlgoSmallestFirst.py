from batsim.batsim import BatsimScheduler, Batsim
#import cProfile
import csv
import json
import math
from procset import ProcSet
import os

from approxAlgoLib import *

class ApproxAlgoSmallestFirst(BatsimScheduler):

    def __init__(self, options):
        super().__init__(options)
        
        # Verify if the input_path was provided
        #assert "container_description_path" in options, "The path to the input files should be given as a CLI option as follows: [pybatsim command] -o \'{\"input_path\":\"path/to/input/files\"}\'"
        #if not os.path.exists(options["container_description_path"]):
        if "container_description_path" not in options:
            assert False, "Could not find input path {}".format(options)#["container_description_path"])

        if "profiles_description_per_machine_path" not in options:
           assert False, "Could not find input path {}".format(options)#["profile_description_per_machine_path"])                

        if "download_info_csv_path" in options:
            self.download_info_csv_path = options["download_info_csv_path"]
        else:
            assert False, "Could not find input path {}".format(options["download_info_csv_path"])

        if "workload_size" in options:
            self.workload_size = options["workload_size"]
        else:
            assert False, "Could not find input path {}".format(options["workload_size"])

        if "random_seed" in options:
            self.random_seed = options["random_seed"]
        else:
            assert False, "Could not find input path {}".format(options["random_seed"])

        # Read and save the external profiles (for containers)
        self.list_of_containers = []
        with open(options["container_description_path"], "r") as f:
            self.container_description = json.load(f)
        for container in self.container_description["profiles"].keys():
            self.list_of_containers.append(container)

        # Read and save the external profiles (for functions per machine)
        with open(options["profiles_description_per_machine_path"], "r") as f:
            self.profile_computation_description = json.load(f)

        #random.seed(self.random_seed)

        self.nb_completed_jobs = 0
        self.nb_jobs = 0
        self.nb_container_downloaded = 0
        self.total_io_mb = 0
        self.total_container_downloaded_mb = 0
        self.total_container_downloaded_mb_expected = 0
        self.total_energy_consumption = 0
        self.platform_total_energy_consumption = 0

        self.notify_already_sent = False
        self.end_of_simulation_asked = False
        
        self.time_next_update = 1.0
        self.update_period = 150
        self.sched_delay = 0.005

        self.jobs_completed = []
        self.jobs_waiting = []
        self.required_containers = []
        self.list_of_machines = []
        self.machines_description = {}
        self.machines_per_cluster = []
        self.cluster_description = {}
        self.mapping_job_container = {}
        self.mapping_machine_container = {}
        self.mapping_original_job_updated_job = {}
        self.mapping_jobs_waiting_machines = {}

        self.mapping_job_container_approx_algo = {}
        self.mapping_job_id = {}
        self.mapping_machine_id = {}
        self.mapping_container_id = {}
        self.mapping_container_job = {}
        self.mapping_energy_consumption_per_job = {}

        self.scheduling_mapping = {}
        self.availableResources = None
        self.openJobs = set()

        self.global_scheduledJobs = []

        self.container_jobs_scheduled = []
        self.container_jobs_executed = []
        self.original_jobs_scheduled = []

        self.list_of_valid_cost = []
        self.list_of_valid_makespan = []
        self.list_of_cost_max_used = []
        self.list_of_makespan_max_used = []
        self.list_of_cost_lp = []
        self.list_of_makespan_lp = []

# ----------------------------- ApproxAlgo -----------------------------------------

    def callsLPAlgo_example(self):
        N = 7
        M = 3
        K = 3

        c =    [[3, 1, 1, 1, 1, 1, 1],
                [3, 1, 1, 1, 1, 1, 1],
                [3, 1, 1, 1, 1, 1, 1]]

        p =    [[33, 12, 13, 19, 17, 14, 14],
                [57, 33, 24, 65, 87, 35, 51],
                [34, 51, 31, 14, 12, 13, 11]]

        d =    [[1, 1, 1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1, 1, 1]]               

        b =    [[1, 1, 1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1, 1, 1]]

        #env =  [0, 0, 0, 0, 0, 0, 0]
        env =  [2, 0, 1, 1, 1, 0, 1]
        mc = [1, 1, 1]

        Cmax = 12
        Tmax = 400

        status, x, e = LP(Cmax, Tmax, M, N, K, c, p, d, b, env, mc)

        #print("Solution of test x: ", x)
        #print("Solution of test y: ", e)

        #x_a, e_a = to_integer_solution(x, M, N, K, c, p, d, b, env)

    #def convertBatsimData(self, queue_of_jobs, list_machines_available):
    def convertBatsimData(self, queue_of_jobs, list_cluster_available):        
        list_of_functions_execution_time = []
        list_of_functions_cost = []
        list_of_containers = []
        list_of_containers_execution_time = []
        list_of_containers_cost = []
        list_of_function_energy_consumption = []
        list_of_containers_energy_consumption = []

        machine_id = 0
        container_id = 0
        print("Cluster description")
        for cluster_id in self.cluster_description:
            function_execution_time_on_machine = []
            function_cost_on_machine = [] # energia
            function_energy_consumption_on_machine = []
            container_execution_time_on_machine = []
            container_cost_on_machine = [] #energia
            container_energy_consumption_on_machine = []

            self.mapping_machine_id[machine_id] = cluster_id
            machine_cpu_speed = self.cluster_description[cluster_id]["speed"].split("Mf")[0]
            machine_name_cluster = self.cluster_description[cluster_id]["cluster"]
            self.mapping_energy_consumption_per_job[machine_id] = {}
            job_id = 0
            for job in queue_of_jobs:
                self.mapping_job_id[job_id] = job.id

                job_per_machine_name = job.profile + "_" + machine_name_cluster
                function_computation_needed = float(self.profile_computation_description["profiles"][job_per_machine_name].get("cpu"))
                function_execution_time = round((function_computation_needed / float(machine_cpu_speed)) / 1000000, 2)
                if (function_execution_time <= 0 ):
                    function_execution_time = 1
                energy_consumption = float(self.profile_computation_description["profiles"][job_per_machine_name].get("energy_consumption")) #* function_execution_time
                self.mapping_energy_consumption_per_job[machine_id][job.id] = energy_consumption
                
                job.profile_dict['cpu'] = function_computation_needed
                job.profile_dict['energy_consumption'] = energy_consumption
                
                function_cost = int(job.profile_dict['io'])
                job_container = job.profile_dict['container']['image'] + '_' + job.profile_dict['container']['tag']
                if job_container not in list_of_containers:
                    list_of_containers.append(job_container)

                function_cost_on_machine.append(function_cost) # Cost is the same for all machines, they are note distubed
                function_energy_consumption_on_machine.append(energy_consumption)

                function_execution_time_on_machine.append(function_execution_time)
                if(self.mapping_container_id.get(job_container) == None):
                    self.mapping_container_id[job_container] = container_id
                    container_id += 1
                self.mapping_job_container_approx_algo[job.id] = job_container

                job_id += 1

            for job_container in list_of_containers:
                container_computation_needed = float(self.container_description["profiles"][job_container]['cpu'])
                container_execution_time = round((container_computation_needed / float(machine_cpu_speed)) / 1000000, 2)
                if (container_execution_time <= 0 ):
                    container_execution_time = 1
                container_cost = int(self.container_description["profiles"][job_container]['size'])

                
                container_cost_on_machine.append(container_cost) # Cost is the same for all machines, they are note distubed
                container_energy_consumption_on_machine.append(0) # they are already computed on the function time
                container_execution_time_on_machine.append(container_execution_time)
            
            list_of_functions_execution_time.append(function_execution_time_on_machine)
            list_of_functions_cost.append(function_cost_on_machine)
            list_of_function_energy_consumption.append(function_energy_consumption_on_machine)
            list_of_containers_execution_time.append(container_execution_time_on_machine)
            list_of_containers_cost.append(container_cost_on_machine)
            list_of_containers_energy_consumption.append(container_energy_consumption_on_machine)

            machine_id += 1

        list_of_containers = []
        for job in queue_of_jobs:
            job_container = job.profile_dict['container']['image'] + '_' + job.profile_dict['container']['tag']
            job_container_in_mapping_id = self.mapping_container_id.get(job_container)
            list_of_containers.append(job_container_in_mapping_id)

        return job_id, machine_id, container_id,  list_of_functions_execution_time, list_of_function_energy_consumption, list_of_containers_execution_time, list_of_containers_energy_consumption, list_of_containers

    def callsLPAlgo(self, queue_of_jobs, list_clusters_available):
        N, M, K, p, c, b, d, env = self.convertBatsimData(queue_of_jobs, list_clusters_available)
        Cmax, Tmax = compute_max_cmax_and_tmax(p, c, b, d, K, M, N)

        print("Aqui", M, N, K)
        #status_new, x_new, e_new, new_cmax, new_tmax, list_of_cmax_used, list_of_tmax_used, list_of_cmax_lp, list_of_tmax_lp = minimize_cmax_and_tmax(Cmax, Tmax, M, N, K, c, p, d, b, env, self.machines_per_cluster)#, optimization_factor)
        status_new, x_new, e_new, new_cmax, new_tmax, list_of_valid_cmax, list_of_valid_tmax, list_of_cmax_used, list_of_tmax_used, list_of_cmax_lp, list_of_tmax_lp = minimize_cmax_and_tmax(Cmax, Tmax, M, N, K, c, p, d, b, env, self.machines_per_cluster)#, optimization_factor)

        if (status_new == 1):
            status, x, e = status_new, x_new, e_new
        Cmax, Tmax = new_cmax, new_tmax

        self.list_of_valid_cost = list_of_valid_cmax
        self.list_of_valid_makespan = list_of_valid_tmax
        self.list_of_cost_max_used = list_of_cmax_used
        self.list_of_makespan_max_used = list_of_tmax_used
        self.list_of_cost_lp = list_of_cmax_lp
        self.list_of_makespan_lp = list_of_tmax_lp

        print("Input matrixes: ")
        print("Energy of functions:")
        print_as_matrix(c)
        print("Energy of Containers:")
        print_as_matrix(d)
        print("Execution time of functions:")
        print_as_matrix(p)
        print("Execution time of containers:")
        print_as_matrix(b)

        print("Integral solution : ")
        print_as_matrix(x_new)
        print("Tmaxes: ", list_of_tmax_lp, new_tmax, Tmax)

        return status_new, x_new, Tmax

    def convertLPSolutionToBatsimFormat(self, lp_solution):

        allocation_dict = {}
        for cluster_id in range(0, len(lp_solution)):
            for job_id in range(0, len(lp_solution[cluster_id])):
                if (lp_solution[cluster_id][job_id] == 1):
                    cluster = self.mapping_machine_id[cluster_id]
                    job = self.mapping_job_id[job_id]
                    if(allocation_dict.get(cluster) == None):
                        allocation_dict[cluster] = [job]
                    else:
                        allocation_dict[cluster].append(job)
                #else:
                #    if (lp_solution[cluster_id][job_id] == 1):

        return allocation_dict
    
    def smallest_first(self, lp_solution_per_cluster, time_limit):
        # To inialize the allocation dict with empty lists
        allocation_dict = {}
        total_number_of_machines = len(self.list_of_machines)
        for i in range(total_number_of_machines):
            allocation_dict[i] = []
        for cluster in lp_solution_per_cluster:

            order = 0 #ascending
            temporary_allocation_queue = self.sort_jobs_by_size(lp_solution_per_cluster[cluster], cluster, order)
            machines_per_cluster = self.cluster_description.get(cluster).get("list_of_machines_id")#.sort()
            machines_per_cluster_id = 0
            total_machines_per_cluster_id = 0
            machines_used = []
            
            machine_name_cluster = self.cluster_description[cluster]["cluster"]
            
            platform_profile_as_function = "platform_" + machine_name_cluster + "_worker"
            print("platform_profile_as_function", platform_profile_as_function)
            energy_per_machine_on_cluster = float(self.profile_computation_description["profiles"][platform_profile_as_function].get("energy_consumption")) 
            print("energy_per_machine_on_cluster: ", energy_per_machine_on_cluster)
            
            while len(temporary_allocation_queue) > 0:
                machine_id = machines_per_cluster[machines_per_cluster_id]
                if (machine_id not in machines_used):
                    machines_used.append(machine_id)
                new_job = temporary_allocation_queue.pop(0)
                allocation_dict[machine_id].append(new_job.id)

                if (machines_per_cluster_id + 1 == len(machines_per_cluster)):
                    machines_per_cluster_id = 0
                else:
                    machines_per_cluster_id += 1
            
            total_machines_per_cluster_id = len(machines_used)
            self.platform_total_energy_consumption += total_machines_per_cluster_id * energy_per_machine_on_cluster
            print("Machines used: ", machines_used)
            print("In cluster, we used: ", total_machines_per_cluster_id)
            print("Energy used here: ", total_machines_per_cluster_id * energy_per_machine_on_cluster)

        return allocation_dict

# ----------------------------- Simulation -----------------------------------------
    def save_json_output_as_csv(self, file_name, json_data):
        header = []
        data = []    

        for key,value in json_data.items():
            header.append(key)
            data.append(value)

        with open(file_name, 'w', encoding='UTF8') as f:
            writer = csv.writer(f)
            writer.writerow(header)

        return
        
    def downloading_container_as_job(self, job):
        """
        Check if the job is a dynamic job representing a container being downloaded.
        """

        is_container = [s for s in self.list_of_containers if s in job.id]
        if (len(is_container) != 0):
            return is_container[0]
        else:
            return None

    def get_layers_from_container(self, job_container):
        """
        Get the layers of a job_cotainer and return as a list.
        """

        list_of_layers = self.container_description.get("profiles").get(job_container).get("layers")
        if(list_of_layers != None):
            return list_of_layers
        else:
            return {}

    def get_layers_total_download_size_from_container(self, job_container):
        """
        Get the layers of a job_cotainer and return as a list.
        """

        total_download_size = self.container_description.get("profiles").get(job_container).get("size")
        if(total_download_size != None):
            return total_download_size
        else:
            return -1

    def list_machines_with_container(self, job_container, check_layers=False):
        """
        List machines that already have job_container
        """

        list_of_layers = self.get_layers_from_container(job_container)
        machine_candidates = []
        scores_machine_container = {}

        # Let's check if any machine available has the job_container
        # or other container with common layers
        for machine in self.list_of_machines: #self.availableResources:
            machine_id = int(str(machine))
            scores_machine_container[machine_id] = 0
            containers_in_machine = self.mapping_machine_container[machine_id]
            # It has the job_container
            if (job_container in containers_in_machine):
                scores_machine_container[machine_id] = 1 #self.get_layers_total_download_size_from_container(job_container)
                machine_candidates.append(machine)

            # Check other container layers, and compute the percetage of matching
            else:
                for container in containers_in_machine:
                    list_of_layers_of_second_container = self.get_layers_from_container(container)
                    for layer in list_of_layers:
                        if layer in list_of_layers_of_second_container:
                            scores_machine_container[machine_id] += list_of_layers_of_second_container.get(layer)
                    
                layers_total_download_size = self.get_layers_total_download_size_from_container(job_container)
                total_number_of_layers_per_container = len(self.get_layers_from_container(job_container))

                if(scores_machine_container[machine_id] != 0 and layers_total_download_size != -1):
                    scores_machine_container[machine_id] /= total_number_of_layers_per_container
                
                # If there is any problem with the container definition, some missing size in the .json file, for example, consider such container as invalid, so size 0
                else:
                    scores_machine_container[machine_id] = 0

        machine_candidates = sorted(scores_machine_container, key=scores_machine_container.get, reverse=True)
        
        return machine_candidates, scores_machine_container

    def get_earliest_submitted_job(self):
        selected_job = None
        for job in self.openJobs:
            if (selected_job == None):
                selected_job = job
            elif (selected_job.submit_time > job.submit_time):
                selected_job = job
        return selected_job

    def get_machines_and_speed(self, machines_resource_description):
        for machine in machines_resource_description:
            self.machines_description[machine["id"]] = machine["properties"]["speed"]

    def get_machines(self, machines_resource_description):
        for machine in machines_resource_description:
            self.list_of_machines.append(machine["id"])

    def get_machines_per_cluster(self, machines_resource_description):
        cluster_id_sequence = 0
        machine_id = 0
        for machine in machines_resource_description:
            machine_speed = machine.get("properties").get("speed")
            cluster_name = machine.get("properties").get("cluster")
            
            new_cluster = False
            for cluster in self.cluster_description:
                if (self.cluster_description[cluster]["cluster"] == cluster_name):
                    new_cluster = True
                    cluster_id = cluster
                    break
            if (new_cluster == False):
                cluster_id = cluster_id_sequence
                cluster_id_sequence += 1

            if (self.cluster_description.get(cluster_id) == None):
                self.cluster_description[cluster_id] = {}
                self.cluster_description[cluster_id]["cluster"] = cluster_name #machine_cluster#number_of_machines_per_speed[machine_speed]
                self.cluster_description[cluster_id]["speed"] = machine_speed#machine.get("properties").get("speed") #machine_speed
                self.cluster_description[cluster_id]["number_of_machines"] = 1 #number_of_machines_per_speed[machine_speed]
                self.cluster_description[cluster_id]["list_of_machines_id"] = [machine_id]
            else:
                self.cluster_description[cluster_id]["number_of_machines"] = self.cluster_description[cluster_id]["number_of_machines"] + 1  #number_of_machines_per_speed[machine_speed]
                self.cluster_description[cluster_id]["list_of_machines_id"].append(machine_id)
            machine_id += 1       
        self.machines_per_cluster = [machine.get("number_of_machines") for machine in self.cluster_description.values()]

    def save_output_as_csv(self, file_name, json_data):
        header = []
        data = []    

        for key,value in json_data.items():
            header.append(key)
            data.append(value)

        with open(file_name, 'w', encoding='UTF8') as f:
            writer = csv.writer(f)
            # write the header
            writer.writerow(header)
            # write the data
            writer.writerow(data)
        return
    
    def attach_original_info_to_lp_allocation(self, approx_algo_allocation_per_cluster):
        original_jobs_approx_algo_allocation_per_cluster = {}
        for cluster in approx_algo_allocation_per_cluster:
            original_jobs_approx_algo_allocation_per_cluster[cluster] = []
            for job_id in approx_algo_allocation_per_cluster.get(cluster):
                job = None
                for open_job in self.openJobs:
                    if open_job.id == job_id:
                        job = open_job
                        break
                original_jobs_approx_algo_allocation_per_cluster[cluster].append(job)
        return original_jobs_approx_algo_allocation_per_cluster
    
    def sort_jobs(self, set_of_jobs):
        """
        It receives a set of jobs (type set()), and return a list with all elementes sorted by id.
        """
        temporary_queue = set_of_jobs.copy()
        sorted_queue = []

        while (len(temporary_queue) > 0):
            selected_job = None
            selected_job_id = None
            queue = iter(temporary_queue)
            for job in queue:
                job_id = int(job.id.split("!")[1])

                if (selected_job == None):
                    selected_job = job
                elif (selected_job_id > job_id):
                    selected_job = job

                selected_job_id = int(selected_job.id.split("!")[1])
            
            # At this point, selected_job is the earlies one (smaller id)
            sorted_queue.append(selected_job)
            temporary_queue.remove(selected_job)

        return sorted_queue
    
    def sort_jobs_by_id(self, set_of_jobs, cluster_id):
        """
        It receives a set of jobs (type set()), and return a list with all elementes sorted by id.
        """
        temporary_queue = set_of_jobs.copy()
        sorted_queue = []

        while (len(temporary_queue) > 0):
            selected_job = None
            selected_job_id = None
            queue = iter(temporary_queue)
            for job in queue:
                job_id = int(job.id.split("!")[1])

                if (selected_job == None):
                    selected_job = job
                elif (selected_job_id > job_id):
                    selected_job = job

                selected_job_id = int(selected_job.id.split("!")[1])
            
            # At this point, selected_job is the earlies one (smaller id)
            sorted_queue.append(selected_job)
            temporary_queue.remove(selected_job)

        return sorted_queue
        
    def sort_jobs_by_size(self, set_of_jobs, cluster_id, order):
        """
        It receives a set of jobs (type set()), and return a list with all elementes sorted by id.
        Order 0 means ascending, and 1 descending.
        """
        temporary_queue = set_of_jobs.copy()
        sorted_queue = []
        sorted_sizes = []

        while (len(temporary_queue) > 0):
            selected_job = None
            selected_job_size = float("inf")
            #selected_job_size = 0
            queue = iter(temporary_queue)
            machine_cpu_speed = self.cluster_description[cluster_id]["speed"].split("Mf")[0]
            machine_name_cluster = self.cluster_description[cluster_id]["cluster"]
            
            for job in queue:
                
                job_per_machine_name = job.profile + "_" + machine_name_cluster
                function_computation_needed = float(self.profile_computation_description["profiles"][job_per_machine_name].get("cpu"))
                job_size = round((function_computation_needed / float(machine_cpu_speed))) #/ 1000000, 2)
                if (order == 0): # ascending order
                    if (job_size <= selected_job_size):
                        selected_job = job
                        selected_job_size = job_size
                else: # descending order
                    if (job_size > selected_job_size):
                        selected_job = job
                        selected_job_size = job_size
            # At this point, selected_job is the earlies one (smaller id)
            sorted_sizes.append(selected_job_size)
            sorted_queue.append(selected_job)
            temporary_queue.remove(selected_job)
        return sorted_queue

    def onSimulationBegins(self):
        """
        Verify if the correct flags has been set when the simulation begins
        """

        assert self.bs.dynamic_job_registration_enabled, "Registration of dynamic jobs must be enabled for this scheduler to work"
        assert self.bs.ack_of_dynamic_jobs == False, "Acknowledgment of dynamic jobs must be disabled for this scheduler to work"
        
        self.bs.register_profiles("w0", self.container_description["profiles"])
        self.bs.register_profiles("w0", self.profile_computation_description["profiles"])
        #self.callsLPAlgo_example()

    def onAfterBatsimInit(self):
        """
        Update the set of Jobs and Resources after the simulation begins
        """

        #self.callsLPAlgo_example()
        self.openJobs = set()
        self.availableResources = ProcSet((0,self.bs.nb_compute_resources-1))
        for availableResource in self.availableResources:
            self.mapping_machine_container[availableResource] = []
            self.mapping_jobs_waiting_machines[availableResource] = []
        self.get_machines_and_speed(self.bs.machines["compute"])
        self.get_machines(self.bs.machines["compute"])
        self.get_machines_per_cluster(self.bs.machines["compute"])

    def onBeforeEvents(self):
        """
        Update Batsim time with some small delay before new events happen.
        """

        if self.bs.time() >= self.time_next_update:
            self.time_next_update = math.floor(self.bs.time()) + self.update_period

    def scheduleJobs(self):
        """
        The decion process. It will check if the machines have containers required by the jobs.
        If not, dybamic jobs will be created, and these jobs will represent the downloading of containers.
        """

        scheduledJobs = []
        while(len(self.openJobs) > 0):
            # Get the allocation decisions
            approx_algo_allocation_per_cluster = None
            if(len(self.openJobs) == self.workload_size):

                open_jobs_sorted = self.sort_jobs(self.openJobs)
                solution_status, lp_solution, computed_tmax = self.callsLPAlgo(open_jobs_sorted, self.cluster_description)
                if (solution_status == 0):
                    break
                approx_algo_allocation_per_cluster = self.convertLPSolutionToBatsimFormat(lp_solution)
            if (approx_algo_allocation_per_cluster == None):
                break
            # Since we have the allocation of a set of tasks, per machine, lets allocate the possible ones, 
            # and put the rest in a waiting list.

            # CacheLocality
            original_jobs_approx_algo_allocation_per_cluster = self.attach_original_info_to_lp_allocation(approx_algo_allocation_per_cluster)
            print("Before calling Biggest First ------------------------- \n", approx_algo_allocation_per_cluster)

            approx_algo_allocation = self.smallest_first(original_jobs_approx_algo_allocation_per_cluster, computed_tmax)
            print("Biggest First solution----------------------------- \n", approx_algo_allocation)
            # Lets do it per machine
            for machine_id in approx_algo_allocation:
                # per job
                #while(len(approx_algo_allocation[machine_id]["allocation"]) > 0):
                #job_id = approx_algo_allocation[machine_id]["allocation"].pop(0)
                
                while(len(approx_algo_allocation[machine_id]) > 0):   
                    job_id = approx_algo_allocation[machine_id].pop(0) 
                    job = None
                    for open_job in self.openJobs:
                        if open_job.id == job_id:
                            job = open_job
                            break

                    # The job is not in OpenJobs anymore, so it should be removed from approx_algo_allocation[machine_id]
                    if job == None:
                        break

                    # Retrieve the cluster we are, from the machine we got
                    cluster_id = None
                    for cluster in self.cluster_description:
                        if machine_id in self.cluster_description[cluster]["list_of_machines_id"]:
                            cluster_id = cluster
                            cluster_job = self.cluster_description.get(cluster).get('cluster')

                    # Retrieve job information
                    job_io_size = job.profile_dict["io"]
                    job_energy_consumption = self.mapping_energy_consumption_per_job[cluster_id][job.id]
                    job_container = job.profile_dict['container']['image'] + "_"  + job.profile_dict['container']['tag']
                    job_container_size = self.container_description["profiles"][job_container]["size"]

                    # Register the new profile and job to update the cpu and energy consumption by the selected
                    # machine and cluster
                    new_profile_name = job.profile + "_" + str(cluster_job)
                    if new_profile_name not in list(self.profile_computation_description['profiles'].keys()):
                        new_profile = {}
                        new_profile[new_profile_name] = self.container_description["profiles"].get(job_container)
                        new_profile[new_profile_name]["cpu"] = job.profile_dict["cpu"]
                        new_profile[new_profile_name]["energy_consumption"] = job.profile_dict["energy_consumption"]
                        self.container_description["profiles"][new_profile_name] = new_profile
                        self.bs.register_profiles("w0", new_profile)

                    # Create a dynamic job
                    updated_job = self.bs.register_job(
                            job.id + "_updated", #job.workload + '!' + job_container + "_job" + str(job.id.split("!")[1]) + "_" + str(self.nb_container_downloaded),
                            1, 
                            2000,
                            new_profile_name, 
                            subtime=None)
                    
                    # This is a new container, register it
                    if (job_container not in self.required_containers):
                        self.required_containers.append(job_container)

                    # Search the best machine available, which means, one with the required container
                    download_reduction = 0
                    check_layers = True
                    machine_candidates, scores_machine_container = self.list_machines_with_container(job_container, check_layers)
                    if (machine_id in machine_candidates):
                        machine = machine_id
                        download_reduction = scores_machine_container[machine]
                    else:
                        break

                    machine = ProcSet((machine_id,machine_id)) # Convert the machine id to a ProcSet
                    # If the container is not on the machine, download it there, before scheduling the job
                    if (job_container != None and 
                        job_container not in self.mapping_machine_container[int(str(machine))]):
                        self.total_container_downloaded_mb_expected += job_container_size
                        new_profile_name = job_container
                        new_size = job_container_size

                        # If there are usefull layers in the allocated machine, create a new profile for such job, with a new delay
                        if (download_reduction != 0):
                            new_profile_name = job_container + '_reduced_' + str(round(download_reduction, 2))
                            new_profile = {}
                            if new_profile_name not in self.container_description["profiles"].keys():
                                new_profile[new_profile_name] = self.container_description["profiles"].get(job_container)
                        
                                if (download_reduction >= 1):
                                    new_computation_required = 1
                                    new_size = 1
                                    
                                else:
                                    new_computation_required = round(new_profile[new_profile_name]["cpu"] - (new_profile[new_profile_name]["cpu"] * download_reduction), 2)
                                    new_size = round(new_profile[new_profile_name]["size"] - (new_profile[new_profile_name]["size"] * download_reduction), 2)

                                new_profile[new_profile_name]["cpu"] = new_computation_required
                                new_profile[new_profile_name]["size"] = new_size
                                
                                self.container_description["profiles"][new_profile_name] = new_profile
                                self.bs.register_profiles("w0", new_profile)
                                #self.bs.mofify_profiles("w0", new_profile)

                        # Create a dynamic job
                        new_job = self.bs.register_job(
                                job.workload + '!' + job_container + "_job" + str(job.id.split("!")[1]) + "_" + str(self.nb_container_downloaded),
                                1, 
                                2000,
                                new_profile_name, 
                                subtime=None)
                    
                        self.container_jobs_scheduled.append(new_job)
                        self.total_container_downloaded_mb += new_size
                        
                        # Allocate the new job to the machine reserved, and add it in the scheduledJobs list
                        new_job.allocation = machine

                        # Check if the machine is available, if not, put in a waiting list
                        if(machine.issubset(self.availableResources) == True):
                            # Allocate the new job to the machine reserved, and add it in the scheduledJobs list
                            scheduledJobs.append(new_job)
                            self.nb_jobs += 2
                            self.nb_container_downloaded += 1
                            self.availableResources -= machine

                        else:
                            self.mapping_jobs_waiting_machines[int(str(machine))].append(new_job)

                        # Save where job should be executed, and what is the container it depends on
                        job.allocation = machine
                        updated_job.allocation = machine
                        self.mapping_original_job_updated_job[job.id] = [job, updated_job.id]
                        self.mapping_job_container[updated_job.id] = [updated_job, new_job.id]
                        
                        # Add the container as it is already executed in the machine.
                        # Then other jobs can see it and plan to be in the same machine
                        container_name = self.downloading_container_as_job(new_job)
                        self.mapping_machine_container[machine_id].append(container_name)
                
                    # Or the container is already in the machine, or the job does not require a container, 
                    # so the job can be scheduled
                    else:
                        job.allocation = machine
                        updated_job.allocation = machine
                        self.mapping_original_job_updated_job[job.id] = [job, updated_job.id]
                        if(machine.issubset(self.availableResources) == False) :
                            #self.mapping_jobs_waiting_machines[int(str(machine))].append(job)
                            self.mapping_jobs_waiting_machines[int(str(machine))].append(updated_job)
                        else:
                            #scheduledJobs.append(job)
                            scheduledJobs.append(updated_job)
                            self.availableResources -= machine

                        # Allocate the new job to the machine reserved, and add it in the scheduledJobs list
                        self.nb_jobs += 1

                    self.openJobs.remove(job)
                    self.total_io_mb += job_io_size
                    self.total_energy_consumption += job_energy_consumption
                    self.availableResources -= machine

            # If all jobs were processed, break the loop to avoid iterating in the first loop for nothing
            #if(len(self.availableResources) == 0):
            #    break

        # Update time
        self.bs.consume_time(self.sched_delay)

        # Send the scheduled jobs to Batsim
        if len(scheduledJobs) > 0:
            self.bs.execute_jobs(scheduledJobs)     

    def onJobSubmission(self, job):
        if (self.downloading_container_as_job(job) == None):
            self.openJobs.add(job)
        self.scheduleJobs()
    

    def onJobCompletion(self, job):
        self.nb_completed_jobs += 1
        self.jobs_completed.append(job)
        machine_id = int(str(job.allocation))

        # If the completed job is a dynamic job (container), we will use the same machine to compute the original job
        # that required such dynamic job.
        container_name = self.downloading_container_as_job(job)
        if (container_name != None or ("_" in job.id and job.id.split("_")[1] == 'updated')):

            job_related_to_dynamic_job = None
            if (container_name != None):
                # Add the container in the machine
                if(container_name not in self.mapping_machine_container[machine_id]):
                    self.mapping_machine_container[machine_id].append(container_name)
                
                # Iterate over the mapping_job_container to find the job related to this dynamic job
                for map_job_container in self.mapping_job_container.items():
                    if(map_job_container[1][1] == job.id):
                        job_related_to_dynamic_job = map_job_container[1][0]

            else:
                # Iterate over the mapping_job_container to find the job related to this dynamic job
                for map_original_job_updated_job in self.mapping_original_job_updated_job.items():
                    if(map_original_job_updated_job[1][1] == job.id):
                        job_related_to_dynamic_job = map_original_job_updated_job[1][0]

            # If some job is found, send it to be executed.
            if (job_related_to_dynamic_job != None):
                scheduledJobs = [job_related_to_dynamic_job]
                self.bs.execute_jobs(scheduledJobs)
        
        # If it was an original job that was completed, we need to free the machine used
        else:
            # Free the mapping_job_container to not waste memory
            if self.mapping_job_container.get(job.id) != None:
                del self.mapping_job_container[job.id]

            # Free resources (machines)
            if (len(self.availableResources) == 0):
                self.availableResources = job.allocation
            else:
                self.availableResources |= job.allocation

            # Iterate over the mapping_jobs_waiting_machines to search jobs waiting for this machine
            if (len(self.mapping_jobs_waiting_machines[machine_id]) != 0):
                scheduledJobs = [self.mapping_jobs_waiting_machines[machine_id].pop(0)]
                self.availableResources -= job.allocation
                self.bs.execute_jobs(scheduledJobs)

        if(len(self.openJobs) != 0):
            self.scheduleJobs()

    def onNoMoreEvents(self):
        if(self.bs.nb_jobs_submitted != 0 and len(self.openJobs) == 0 and len(self.jobs_completed) == self.bs.nb_jobs_submitted and self.notify_already_sent == False):
            self.notify_already_sent = True
            self.bs.notify_registration_finished()

            output_data = {
                "total_io": self.total_io_mb,
                "total_container_data_downloaded_mb_expected": self.total_container_downloaded_mb_expected,
                "total_container_data_downloaded_mb": self.total_container_downloaded_mb, 
                "nb_different_required_containers": len(self.required_containers),
                "nb_container_downloaded": self.nb_container_downloaded,
                "total_io_and_container_data_downloaded": self.total_io_mb + self.total_container_downloaded_mb
            }
            
            self.save_output_as_csv(self.download_info_csv_path + "out_download_data_info.csv", output_data)
            energy_consumption_info_csv_path = self.download_info_csv_path
            output_data_energy = {
                "output_data_energy": self.total_energy_consumption, #round(self.total_energy_consumption * 1000, 2), # converting to Joules
                "platform_output_data_energy": self.platform_total_energy_consumption,
                "total_output_data_energy": self.platform_total_energy_consumption + self.total_energy_consumption,
            }

            self.save_output_as_csv(energy_consumption_info_csv_path + "out_energy_consumption.csv", output_data_energy)
            #header = ["max_cost_used", "max_makespan_used", "max_cost_lp", "max_makespan_lp"]
            header = ["valid_cost", "valid_makespan", "max_cost_used", "max_makespan_used", "max_cost_lp", "max_makespan_lp"]
            with open(self.download_info_csv_path + "out_valid_solutions.csv", 'w', encoding='UTF8') as f:
                writer = csv.writer(f)
                writer.writerow(header)
                for i in range(0, len(self.list_of_cost_max_used)):
                    #row = [self.list_of_cost_max_used[i], self.list_of_makespan_max_used[i], self.list_of_cost_lp[i], self.list_of_makespan_lp[i]]
                    row = [self.list_of_valid_cost[i], self.list_of_valid_makespan[i], self.list_of_cost_max_used[i], self.list_of_makespan_max_used[i], self.list_of_cost_lp[i], self.list_of_makespan_lp[i]]
                    writer.writerow(row)