from evalys import visu
from evalys.jobset import JobSet
from evalys.visu.gantt import NOLABEL  # working with evalys>=4.0.6
import matplotlib.pyplot as plt
import pandas as pd

def andrei_labeler(job):
    return str(job['jobID']) + '\nA'

def main():

    df = pd.read_csv('out_jobs.csv')
    df['job_id'] = ''
    df.to_csv('out_jobs2.csv')

    js = JobSet.from_csv("out_jobs2.csv")
    js.plot(with_details=True)
    plt.savefig('gantt.png')
    plt.show()
    #visu.plot_gantt(js, labeler=NOLABEL)
main()

"""
# -*- coding: utf-8 -*-

def andrei_labeler(job):
    return str(job['jobID']) + '\nA'

# WORKING visualizations
visu.plot_lifecycle(js, xscale='time')

# js.reset_time()
visu.plot_gantt(js, labeler=andrei_labeler)
visu.plot_gantt(js, labeler=NOLABEL)  # working with evalys>=4.0.6

# impacted by foreign change (introduction of legend_label)
# visu.plot_series(js, name='queue', legend_label='queue')
# visu.plot_series(js, name='utilization', legend_label='utilization')

# NOT WORKING, need debuging ¯\_(ツ)_/¯
# visu.plot_details(js, xscale='time')
"""