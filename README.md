# *FOA-Energy* A Multi Objective Scheduling Policy for Serverless

This is the repository to reproduce the experiments of the scientific paper: *A Multi-objective Energy-Aware Scheduling Policy for Serverless-based Edge-Cloud Continuum* submitted to the [Future Generation Computer Systems](https://www.sciencedirect.com/journal/future-generation-computer-systems).

In this Git repository, you will find the code to generate the required inputs (workloads and platforms), to run the experiments, the analysis and to test pre-designed scenarios. 

## Abstract 

We provide the source code implementation of all the scheduling policies proposed in the paper, as well as the source code of the simulation experiments used to evaluate our approach. Then, the reader can 
- (i) generate and reproduce the experiments described in the paper for both evaluated scheduling policies, 
- (ii) reproduce the analysis presented in the paper, 
- (iii) generate and run their own workloads, platforms, and experiments, 
- (iv) modify and exploit the linear program that is the basis of our proposed scheduling policy, *FOA*, and 
- (v) test of pre-designed scenarios.

## Description

### Check-list (artifact meta information)


  - **Program** :
    - (i) The scheduling policies' source code (including *FOA-Energy*); 
    - (ii) The scripts to install the experimental environment, to generate the inputs, and to run the experiments; 
    - (iii) The script to pre-process the simulated outputs; 
    - (iv) The script to run the analysis, in addition to a reproducible document (Jupyter Notebook); and 
    - (v) The script for testing pre-designed scenarios.
  
  - **Compilation** : GCC

  - **Data set** : Workload models, container descriptions, platform models, and experiment descriptions. See more details below .
  
  - **Run-time environment** : Python 3.8.
  
  - **Hardware** : Various x86 or x64 CPUs. 4 or 6GB of RAM.
  
  - **Experiment workflow** : 
      - **Step 1**: Installation
      - **Step 2**: Inputs generation 
      - **Step 3**: Execution of simulations; 
      - **Step 4**: Preprocessing of simulation outputs;
      - **Step 5**: Testing of results;
      - **Step 6**: Analysis of results.

  
  - **Output**:    
      - **Step 1**:  None
      - **Step 2**:  Generated workloads and platforms (in JSON format)
      - **Step 3**:  Output of the simulations (in CSV format). 
      - **Step 4**:  Preprocessed simulation results (in CSV format)
      - **Step 5**:  Messages with status.
      - **Step 6**:  Figures (in PNG and PDF format).
         
  - **Experiment customization**:  See below.
  - **Publicly available?**:  Yes.
  - **Vocabulary**:  At the implementation level, *FOA* is called *approxAlgo*  and *K8S ImageLocality* is called *kubernetesAlgo* .

## Organization

The Git repository is structured by the following main directories:

  - ```analysis```:  It contains all the source material to process the simulated outputs and to perform the analysis. Also, the figures generated are saved there;
  
  - ```experiments```:  It contains all the source material to generate and run the simulations, including *FOA*'s linear program. % source code that is the basis of our proposed scheduling algorithm, *FOA*. 
  The directories inside this folder are:
  
      - ```scripts```:  it contains all scripts necessary to execute the experiments;
      
      - ```simulations```:  it contains the files and directories related to the simulations. The directories are: ```exp_out``` , ```platforms``` , ```schedulers```  and ```workloads```.
      
      - ```results```:  it contains all simulation results grouped by scheduling policies and analysis parameters.
      
      - ```tests```:  it contains the results used as a baseline for testing the pre-designed experiments. The outputs of the simulations are deterministic, so we test them by comparing the outputs of the new simulations with validated sets of experiments: (a) ```unit_test```:  a small set of 6 experiments; and (b) ```paper```:  the complete set of experiments performed in this paper.
      
## Hardware dependencies

Any modern x86 or x64 CPU is appropriate to execute the experiments. It is advised, however, that the system has at least 6GB of RAM since some experiments consume significant amounts of memory.

## Software Dependencies

The main software requirements are a Linux distribution (preferably Ubuntu or Debian), the GCC C compiler and Python 3.8. Below is presented a list with the specific software requirements:

- [Git](https://git-scm.com/):  a version control system.
- [Nix](https://nixos.org) (weak requirement): a multiplatform packet manager (run in Linux, Windows with WSL, and MacOS).

- [R](https://cran.r-project.org/) (version 4.1.2): Used for generating the plots. Necessary packages: dplyr, tidyr, ggplot2, data_table, gridExtra, patchwork, RColorBrewer, scales.
    
- [Python 3](https://www.python.org/downloads/) (version 3.10.8): Used for modeling and executing the LP, and extracting data. Necessary libraries: ipython, pyyaml, numpy, pandas, matplotlib, networkx, python-mip.

- [Batsim](https://batsim.readthedocs.io): Batsim is a scientific simulator to analyze batch schedulers. Batch schedulers — or Resource and Jobs Management Systems, RJMSs — are systems that manage resources in large-scale computing centers, notably by scheduling and placing jobs. Necessary extensions: Batexp, Pybatsim, Robin.

- [Jupyter Lab](https://jupyter.org/): JupyterLab is a web-based interactive development environment for notebooks, code, and data.

## Datasets

Except for the container description file, all files here are generated with the scripts available. The container description file is generated after benchmarks that are not in this project's scope.
 
  - Containers description;
  - Workload models;
  - Platform descriptions;
  - Experiment descriptions.
 
## Installation and Execution

After the repository is downloaded and the software dependencies are installed, run the script that manages the whole workflow, `run_all.sh`.

> We remark that it automatically installs Batsim, PyBatsim, and the R and Python packages. But the installation of Python, R and Nix are required.

Step by step:

1. Install [Git](https://git-scm.com/)
2. Install [Nix](https://nixos.org):
3. Install [Python 3](https://www.python.org/downloads/)
> To install manually the Python libraries:
>
> ```$ pip3 install csv json math cProfile hashlib mip numpy itertools networkx matplotlib```
4. Install [R](https://cran.r-project.org/)
> To install manually the R packages:
>
> ```$ R```
>
> ```> install.packages(c("dplyr", "tidyr", "ggplot2", "data.table", "gridExtra", "patchwork", "RColorBrewer"))```
5. Install [JupyeterLab](https://jupyter.org/)

6. Clone (download) this repository: 

```$ git clone https://gitlab.com/andersonandrei/foa-a-multi-objective-scheduling-policy-for-serverless/```

7. To execute the main script, from the root directory: 

```$ cd foa-a-multi-objective-scheduling-policy-for-serverless/experiments/```

```$ bash ./run_all.sh```
 
## Experiment workflow

In this section, we explain what each workflow step does and how to execute them separately. The commands below are assuming that all are going to be executed from the folder ```experiments/``` . The majority of the steps use the same input file, [parameters.yaml](https://gitlab.com/andersonandrei/foa-a-multi-objective-scheduling-policy-for-serverless/-/tree/main/experiments/parameters.yaml) , which specifies paths and parameter values for the experiments. For simplicity, we provide a second input file, [parameters_small.yaml](https://gitlab.com/andersonandrei/foa-a-multi-objective-scheduling-policy-for-serverless/-/tree/main/experiments/parameters_small.yaml) , for a first installation and execution. It produces a small set of 6 experiments that may not take more than 5 minutes for being executed and tested. Besides, the first installation may take longer.

### **Step 1**:  Installation, 

The script [run_all.sh](https://gitlab.com/andersonandrei/foa-a-multi-objective-scheduling-policy-for-serverless/-/tree/main/experiments/run_all.sh) inside the folder ```experiments/``` will at first install the required environment. It can also be done by:

```nix-build environment.nix```


### **Step 2**:  Inputs generation, 

The script [generate_experiments.py](https://gitlab.com/andersonandrei/foa-a-multi-objective-scheduling-policy-for-serverless/-/tree/main/experiments/scripts/generate_experiments.py) inside the folder ```experiments/scripts/```  automatically:
  - Generates the workloads;
  - Generates the platforms;
  - Generates the experiment descriptions;

To execute it:

```python3 scripts/generate_experiments.py parameters.yaml```

> **Customization**: The parameters that define the set of experiments are inside the [parameters.yaml](https://gitlab.com/andersonandrei/foa-a-multi-objective-scheduling-policy-for-serverless/-/tree/main/experiments/parameters.yaml) file. They are:
  - scheduling_policies: ["kubernetesAlgo", "approxAlgo"]
  - platform_sizes: [100, 300, 500]
  - workload_sizes: [200, 600, 1000]
  - heterogeneity_levels: [3, 5, 7]
  - random_seeds: [i for i in range(0, 150, 5)]
 
### **Step 3**:  Execution of Experiments

The script [run_experiments.py](https://gitlab.com/andersonandrei/foa-a-multi-objective-scheduling-policy-for-serverless/-/tree/main/experiments/scripts/run_experiments.py) inside the folder ```experiments/scripts/```  automatically:
  - Installs the environment for simulations;
  - Execute the simulations;

To execute it:
```bash scripts/run_experiments.sh```

> **Customization**: It uses the variable `EXP_DESCRIPTION_PATH` to search the experiments to be executed. If the directory is modified, this variable should be updated.

### **Step 4**:  Preprocessing of simulation outputs

The preprocessing of the simulation outputs groups all output files by the scheduling policies and the metrics that are analyzed. It also retrieves timestamps from the logs of the simulations and converts them to CSV files. The script is the [pre_process_outputs.py](https://gitlab.com/andersonandrei/foa-a-multi-objective-scheduling-policy-for-serverless/-/tree/main/experiments/scripts/pre_process_outputs.py), located in the folder ```experiments/scripts/``` . To execute it:

```python3 scripts/pre_process_outputs.py parameters.yaml```

### **Step 5**:  Tests

The script [run_tests.py](https://gitlab.com/andersonandrei/foa-a-multi-objective-scheduling-policy-for-serverless/-/tree/main/experiments/scripts/run_tests.py), inside the folder ```experiments/scripts/```, executes one of the two tests available, performed through comparisons with expected validated results: 

  - ```unit_test```:  it was designed for the small scenario that is produced by the [parameters_small.yaml](https://gitlab.com/andersonandrei/foa-a-multi-objective-scheduling-policy-for-serverless/-/tree/main/experiments/parameters_small.yaml)  input file. Its baseline results are located at [tests/unit_test/](https://gitlab.com/andersonandrei/foa-a-multi-objective-scheduling-policy-for-serverless/-/tree/main/tests/unit_test//)  ;
  - ```paper```:  it was designed for the whole paper experiments, produced by the [parameters.yaml](https://gitlab.com/andersonandrei/foa-a-multi-objective-scheduling-policy-for-serverless/-/tree/main/experiments/parameters.yaml) input file. Its baseline results are located at [tests/paper/](https://gitlab.com/andersonandrei/foa-a-multi-objective-scheduling-policy-for-serverless/-/tree/main/tests/paper/) .
  
They can be executed through the following commands:

```python3 scripts/run_tests.py parameters_small.yaml```

> **Customization**: To change the scenario that will be tested, modify the parameters below in the [parameters.yaml](https://gitlab.com/andersonandrei/foa-a-multi-objective-scheduling-policy-for-serverless/-/tree/main/experiments/parameters.yaml) or [parameters_small.yaml](https://gitlab.com/andersonandrei/foa-a-multi-objective-scheduling-policy-for-serverless/-/tree/main/experiments/parameters.yaml). The ```unit_test``` is identified by the ID **0** and the ```paper``` by the ID **1**.

- current_results_path: "../results/"
- unit_test_results_path: "../tests/unit_test/"
- paper_results_test_path: "../tests/paper/"
- scheduling_policies_to_test: ["approxAlgo", "kubernetesAlgo"]
- metrics_to_test: ["out_download_data_info", "out_jobs", "out_schedule", "out_valid_solutions"]
- test_id: 0
 
### **Step 6**:  Analysis of Results

We used R to process and produce the figures presented in the paper. The script is called [analysis.r](https://gitlab.com/andersonandrei/foa-a-multi-objective-scheduling-policy-for-serverless/-/tree/main/analysis/analysis.r). In addition, there is a reproducible document (a Jupyter Notebook), [analysis.ipynb](https://gitlab.com/andersonandrei/foa-a-multi-objective-scheduling-policy-for-serverless/-/tree/main/analysis/analysis.ipynb), to do the same, but in an interactive way, allowing easy modifications for further investigation and discussions. The script and the reproducible document are in the same repository ```analysis/``` , and both should be executed from there. To run them, it is necessary to use the following commands from the root repository (not necessarily in sequence):

To run the ```analysis.r```  script:

```Rscript analysis.r```
 
To run the reproducible document:

```jupyter-lab analysis.ipynb```

## Evaluation and expected result

### **Outputs of Step 1**

A Nix message concluding the installation of the environment. It is possible to check if the environment was installed corrected accessing each one of them:

```$ nix-env environment.nix -A batshell --pure```

```$ batsim ```
> version expected: 4.0.0

```$ pybatsim ```
> version expected: 3.2.0

```$ robin ```
> version expected: 1.2.0

```$ exit```

```$ nix-env environment.nix -A pyshell --pure``

```$ python3 ```

```$ import yaml```

```$ exit```
> Execute the same `import` command for the other python3 libraries.

```$ nix-env environment.nix -A rshell --pure```

```$ R ```

```$ library(tidyr) ```

```$ exit```
> Execute the same library() command for the other R libraries.


### **Outputs of Step 2**

The outputs of step 1 are, respectively to the order they were presented (see [Experiment Workflow](https://gitlab.com/andersonandrei/foa-a-multi-objective-scheduling-policy-for-serverless/-/tree/main/experiment-workflow) ):

- Workload files in JSON format, in the folder [experiments/simulations/workloads](https://gitlab.com/andersonandrei/foa-a-multi-objective-scheduling-policy-for-serverless/-/tree/main/experiments/simulations/workloads/) ;

- Platform files in XML format, in the folder [experiments/simulations/platforms](https://gitlab.com/andersonandrei/foa-a-multi-objective-scheduling-policy-for-serverless/-/tree/main/experiments/simulations/platforms/) ;

- Experiment description files in JSON format, in the folder [experiments/simulations/exp_description](https://gitlab.com/andersonandrei/foa-a-multi-objective-scheduling-policy-for-serverless/-/tree/main/experiments/simulations/exp_description/) ;

### **Outputs of Step 3**

The outputs of step 3 are folders for each experiment executed, located in [experiments/simulations/exp_out/](https://gitlab.com/andersonandrei/foa-a-multi-objective-scheduling-policy-for-serverless/-/tree/main/experiments/simulations/exp_out/) , with Batsim outputs (see Batsim's documentation at ```https://batsim.readthedocs.io``` .

### **Outputs of Step 4**

The outputs of step 2 are  CSV files grouped in sub-folders - by the scheduling policies and the parameters analyzed - in the path [experiments/results/](https://gitlab.com/andersonandrei/foa-a-multi-objective-scheduling-policy-for-serverless/-/tree/main/experiments/results/).

### **Outputs of Step 5**

The outputs of step 5 is a message with the status of the tests. `Success` or a `[list]` with the experiments that failed.

### **Outputs of Step 6**

The outputs of step 3 are the results of the analysis of the simulations, 
figures in PDF and PNG format, and CSV files, in the folder [analysis/](https://gitlab.com/andersonandrei/foa-a-multi-objective-scheduling-policy-for-serverless/-/tree/main/analysis/) .